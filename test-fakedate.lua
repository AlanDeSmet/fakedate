#! /usr/bin/lua

-- This file is part of fakedate, an alternate Playdate SDK for testing.
-- Copyright 2022 Alan De Smet
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

function dirname(path)
	while #path > 0 and string.sub(path, -1) == "/" do
		path = string.sub(path, 1, #path-1)
	end
	possible = path:match("(.*[^/])/")
	if possible ~= nil then
		return possible.."/"
	end
	return ""
end

package.path = dirname(arg[0]).."?.lua;"..package.path
package.path = dirname(arg[0]).."src/?.lua;"..package.path
lu = require("luaunit")

-- We can't use fakedatestrict, as it removes things luaunit relies on
require("fakedate")

function test_table_create()
	local newtab = table.create(10,0)
	lu.assertEquals(newtab, {})
	newtab = table.create(0,10)
	lu.assertEquals(newtab, {})
	newtab = table.create(10,10)
	lu.assertEquals(newtab, {})
end

function test_table_getsize()
	local as, hs
	as, hs = table.getsize({})
	lu.assertEquals({as,hs}, {0,1}) -- hashSize appears to always be at least 1
	as, hs = table.getsize({a=10,b=20,c=3})
	lu.assertEquals({as,hs}, {0,4})
	as, hs = table.getsize({10,20,30})
	lu.assertTrue((as+hs) >= 3)
end

function table_test_copy_shallow_only(copyfunc)
	local root = {}
	lu.assertFalse(rawequal(root, copyfunc(root)))

	root = {3,4,5}
	local copy = copyfunc(root)
	lu.assertFalse(rawequal(root, copy))
	lu.assertEquals(copy,root)

	root = {foo=10, bar=99, qux="fred"}
	local copy = copyfunc(root)
	lu.assertFalse(rawequal(root, copy))
	lu.assertEquals(copy,root)
end

function test_table_deepcopy()
	table_test_copy_shallow_only(table.deepcopy)
	local sub = {9,8,7}
	local root = {foo=10, bar=sub, qux="fred"}
	local copy = table.deepcopy(root)
	lu.assertFalse(rawequal(root, copy))
	lu.assertEquals(copy,root)
	lu.assertFalse(rawequal(root.bar, copy.bar))
	lu.assertEquals(copy.bar,root.bar)

	root = {
		digits = { 0,9,8,7,6,5,4,3,2,1},
		deep = {
			next_level_1 = {
				deeper = { "very deep", "so deep" },
				name = "Just a name",
			},
			next_level_2 = {
				maybe = { "yes","no"}
			}
		}
	}
	local copy = table.deepcopy(root)
	lu.assertFalse(rawequal(root, copy))
	lu.assertEquals(copy,root)
	lu.assertFalse(rawequal(root.digits, copy.digits))
	lu.assertFalse(rawequal(root.deep, copy.deep))
	lu.assertFalse(rawequal(root.deep.next_level_1, copy.deep.next_level_1))
	lu.assertFalse(rawequal(root.deep.next_level_1.deeper, copy.deep.next_level_1.deeper))
	lu.assertFalse(rawequal(root.deep.next_level_2.maybe, copy.deep.next_level_2.maybe))

    lu.assertErrorMsgContains(
		'The argument passed to table.deepcopy() must be a table (argument passed was a boolean)',
		table.deepcopy,
		true)
    lu.assertErrorMsgContains(
		'The argument passed to table.deepcopy() must be a table (argument passed was a number)',
		table.deepcopy,
		99)
    lu.assertErrorMsgContains(
		'The argument passed to table.deepcopy() must be a table (argument passed was a string)',
		table.deepcopy,
		"hi!")
end

function test_table_shallowcopy()
	table_test_copy_shallow_only(table.shallowcopy)

	local root = {}
	local sub = {9,8,7}
	root = {foo=10, bar=sub, qux="fred"}
	local copy = table.shallowcopy(root)
	lu.assertFalse(rawequal(root, copy))
	lu.assertEquals(copy,root)
	lu.assertTrue(rawequal(root.bar, copy.bar))

    lu.assertErrorMsgContains( 'table expected', table.shallowcopy, true)
    lu.assertErrorMsgContains( 'table expected', table.shallowcopy, 99)
    lu.assertErrorMsgContains( 'table expected', table.shallowcopy, "hi!")

	root = {foo=10, bar=20, qux=30}
	local dst = {another=99, bar=88}
	local olddst = dst
	copy = table.shallowcopy(root, dst)
	lu.assertTrue(rawequal(dst, copy))
	lu.assertTrue(rawequal(dst, olddst))
	lu.assertEquals(copy, {foo=10, bar=20, qux=30, another=99})
end

function test_table_indexOfElement_present()
	lu.assertEquals(table.indexOfElement({"a","an","aardvark", "bacon", "aardman", "animations"}, "aardman"), 5)
end
function test_table_indexOfElement_duplicates()
	lu.assertEquals(table.indexOfElement({"a","an","aardvark", "bacon", "aardman", "bacon", "bacon", "spam"}, "bacon"), 4)
end
function test_table_indexOfElement_missing()
	lu.assertIsNil(table.indexOfElement({"a","an","aardvark", "bacon", "aardman", "animations"}, "aardma"),nil)
end
function test_table_indexOfElement_empty_table()
	lu.assertIsNil(table.indexOfElement({}, "aardman"),nil)
end

function test_table_indexOfElement_mixed_types()
	lu.assertEquals(table.indexOfElement({"a","an","aardvark", "bacon", 3, "aardman", 10}, 3),5)
	lu.assertEquals(table.indexOfElement({"a","an","aardvark", "bacon", 3, "aardman", 10}, "aardman"),6)
end

function test_table_indexOfElement_nil()
    lu.assertErrorMsgContains(
		'indexOfElement can not check for elements of type nil',
		table.indexOfElement,
		{"a","b","c"},
		nil)
end

--------------------------------------------------------------------------------
--
-- playdate.geometry.point
--
function test_playdate_geometry_point_new()
	pt = playdate.geometry.point.new(3.14, 99.99)
	lu.assertEquals(pt.x, 3.14)
	lu.assertEquals(pt.y, 99.99)
	lu.assertEquals(pt.dx, nil)
end
function test_playdate_geometry_point_offset()
	local pt = playdate.geometry.point.new(3.14, 99.99)
	local pt2 = pt:offset(1,2)
	lu.assertAlmostEquals(pt.x, 4.14, 0.001)
	lu.assertAlmostEquals(pt.y, 101.99, 0.001)
	lu.assertAlmostEquals(pt, pt2)
end
function test_playdate_geometry_point_offsetBy()
	local pt = playdate.geometry.point.new(3.14, 99.99)
	local pt2 = pt:offsetBy(1,2)
	lu.assertEquals(pt.x, 3.14)
	lu.assertEquals(pt.y, 99.99)
	lu.assertAlmostEquals(pt2.x, 4.14, 0.001)
	lu.assertAlmostEquals(pt2.y, 101.99, 0.001)
	lu.assertNotEquals(pt, pt2)
end
function test_playdate_geometry_point_squaredDistanceToPoint()
	local pt1 = playdate.geometry.point.new(10.0, 20.0)
	local pt2 = playdate.geometry.point.new(13.0, 16.0)
	lu.assertEquals(pt1:squaredDistanceToPoint(pt2), 25.0)
	lu.assertEquals(pt2:squaredDistanceToPoint(pt1), 25.0)
end
function test_playdate_geometry_point_distanceToPoint()
	local pt1 = playdate.geometry.point.new(10.0, 20.0)
	local pt2 = playdate.geometry.point.new(13.0, 16.0)
	lu.assertEquals(pt1:distanceToPoint(pt2), 5.0)
	lu.assertEquals(pt2:distanceToPoint(pt1), 5.0)
end
function test_playdate_geometry_point_copy()
	local pt1 = playdate.geometry.point.new(10.0, 20.0)
	local pt2 = pt1:copy()
	lu.assertFalse(rawequal(pt1,pt2))
	lu.assertEquals(pt1.x, 10)
	lu.assertEquals(pt1.y, 20)
	lu.assertEquals(pt2.x, 10)
	lu.assertEquals(pt2.y, 20)
	pt1.x = 99
	pt2.y = 101
	lu.assertEquals(pt1.x, 99)
	lu.assertEquals(pt1.y, 20)
	lu.assertEquals(pt2.x, 10)
	lu.assertEquals(pt2.y, 101)
end
function test_playdate_geometry_point_add()
	local pt1 = playdate.geometry.point.new(10.0, 20.0)
	local function doadd(x,y)
		local a = x + y
	end
    lu.assertErrorMsgContains(
		"bad argument #2 to 'add' (playdate.geometry.vector2D expected, got playdate.geometry.point)",
		doadd, pt1, pt1)
    lu.assertErrorMsgContains(
		"bad argument #2 to 'add' (playdate.geometry.vector2D expected, got number)",
		doadd, pt1, 99)
	local v = playdate.geometry.vector2D.new(200,300)
	local ptout = pt1+v
	lu.assertEquals(ptout.x, 210)
	lu.assertEquals(ptout.y, 320)
end
function test_playdate_geometry_point_sub()
	local PT = playdate.geometry.point.new
	local VEC = playdate.geometry.vector2D.new

    lu.assertErrorMsgContains(
		"The right-hand argument to playdate.geometry.point's - operator should be another point (returning a vector), or a playdate.geometry.vector2D",
		function(x,y) local a = x-y end, PT(10,20), {})

	lu.assertEquals(PT(10,20) - PT(5, 30), VEC(5, -10))
	lu.assertEquals(PT(10,20) - VEC(4, -6), PT(6, 26))

end
function test_playdate_geometry_point_eq()
	local PT = playdate.geometry.point.new
	local function doeq(x,y)
		local a = x == y
	end
	local v = playdate.geometry.vector2D.new(1,2)
	local pt1 = PT(1,2)
    lu.assertErrorMsgContains(
		"bad argument #2 to 'eq' (playdate.geometry.point expected, got playdate.geometry.vector2D)",
		doeq, pt1, v)
		-- Lua won't attempt __eq unless both types are table or userdata
    --lu.assertErrorMsgContains(
	--	"bad argument #2 to 'eq' (playdate.geometry.point expected, got number)",
	--	doeq, pt1, 99)
	lu.assertTrue( PT(1,1) == PT(1.0, 1.0))
	lu.assertFalse(PT(1,1) == PT(1, 0))
	lu.assertFalse(PT(1,1) == PT(0, 1))
	lu.assertFalse(PT(1,1) == PT(0, 0))
end

function test_playdate_geometry_point_tostring()
	local PT = playdate.geometry.point.new
	lu.assertEquals(tostring(PT(1,99)), "(1, 99)")
	lu.assertEquals(tostring(PT(3.14,-99.1)), "(3.14, -99.1)")
end

--------------------------------------------------------------------------------
--
-- playdate.geometry.vector2D
--
function test_playdate_geometry_vector2D_new()
	local v = playdate.geometry.vector2D.new(23,45)
	lu.assertEquals(v.dx, 23)
	lu.assertEquals(v.dy, 45)
	lu.assertEquals(v.x, 23)
	lu.assertEquals(v.y, 45)

	v.dx = 100
	v.dy = 200
	lu.assertEquals(v.dx, 100)
	lu.assertEquals(v.dy, 200)
	lu.assertEquals(v.x, 100)
	lu.assertEquals(v.y, 200)

	v.x = 9
	v.y = 8
	lu.assertEquals(v.dx, 9)
	lu.assertEquals(v.dy, 8)
	lu.assertEquals(v.x, 9)
end
function test_playdate_geometry_vector2D_tostring()
	local V = playdate.geometry.vector2D.new
	lu.assertEquals(tostring(V(1,99)), "(1, 99)")
	lu.assertEquals(tostring(V(3.14,-99.1)), "(3.14, -99.1)")
end

--------------------------------------------------------------------------------
--
-- playdate.geometry.rect
--
function test_playdate_geometry_rect_new()
	local r = playdate.geometry.rect.new(10,20,30,40)
	lu.assertEquals(r.x, 10)
	lu.assertEquals(r.y, 20)
	lu.assertEquals(r.width, 30)
	lu.assertEquals(r.height, 40)
	
	lu.assertEquals(r.left, 10)
	lu.assertEquals(r.top, 20)
	lu.assertEquals(r.right, 40)
	lu.assertEquals(r.bottom, 60)

	r.x = 1
	r.y = 2
	r.width=3
	r.height=4
	lu.assertEquals(r.x, 1)
	lu.assertEquals(r.y, 2)
	lu.assertEquals(r.width, 3)
	lu.assertEquals(r.height, 4)
	
	lu.assertEquals(r.left, 1)
	lu.assertEquals(r.top, 2)
	lu.assertEquals(r.right, 4)
	lu.assertEquals(r.bottom, 6)
end

function test_playdate_geometry_rect_read_only()
	local r = playdate.geometry.rect.new(10,20,30,40)
    lu.assertErrorMsgContains(
		"Could not set value for 'left' on playdate.geometry.rect",
		function(x) x.left = 1 end,
		r)
    lu.assertErrorMsgContains(
		"Could not set value for 'top' on playdate.geometry.rect",
		function(x) x.top = 1 end,
		r)
    lu.assertErrorMsgContains(
		"Could not set value for 'right' on playdate.geometry.rect",
		function(x) x.right = 1 end,
		r)
    lu.assertErrorMsgContains(
		"Could not set value for 'bottom' on playdate.geometry.rect",
		function(x) x.bottom = 1 end,
		r)
end

function test_playdate_geometry_rect_eq()
	local newrect = playdate.geometry.rect.new
	lu.assertTrue(newrect(1,2,3,4) == newrect(1,2,3,4))
	lu.assertFalse(newrect(1,2,3,4) == newrect(2,2,3,4))
	lu.assertFalse(newrect(1,2,3,4) == newrect(1,1,3,4))
	lu.assertFalse(newrect(1,2,3,4) == newrect(1,2,4,4))
	lu.assertFalse(newrect(1,2,3,4) == newrect(1,2,3,3))
    lu.assertErrorMsgContains(
		"bad argument #2 to 'eq' (playdate.geometry.rect expected, got playdate.geometry.point)",
		function() return newrect(1,2,3,4) == playdate.geometry.point.new(1,2) end)
end

--------------------------------------------------------------------------------
--
-- misc
--

function test_setScale_getScale()
	local get = playdate.display.getScale
	local set = playdate.display.setScale
	set(1)
	lu.assertEquals(get(), 1)
	set(2)
	lu.assertEquals(get(), 2)
	set(3)
	lu.assertEquals(get(), 2)
	set(4)
	lu.assertEquals(get(), 4)
	set(7)
	lu.assertEquals(get(), 4)
	set(8)
	lu.assertEquals(get(), 8)
	set(99)
	lu.assertEquals(get(), 8)
end

function test_playdate_display_get_sizes()
	playdate.display.setScale(1)
	lu.assertEquals(playdate.display.getWidth(), 400)
	lu.assertEquals(playdate.display.getHeight(), 240)
	lu.assertEquals(playdate.display.getSize(), 400,240)
	lu.assertEquals(playdate.display.getRect(), playdate.geometry.rect.new(0,0,400,240))
	playdate.display.setScale(8)
	lu.assertEquals(playdate.display.getWidth(), 50)
	lu.assertEquals(playdate.display.getHeight(), 30)
	lu.assertEquals(playdate.display.getSize(), 50,30)
	lu.assertEquals(playdate.display.getRect(), playdate.geometry.rect.new(0,0,50,30))
end

function test_various_time_functions()
	local s = 999999
	local ms = 999999
	playdate.display.setRefreshRate(10)
	lu.assertEquals(playdate.getCurrentTimeMilliseconds(), 0)
	lu.assertEquals(playdate.getElapsedTime(), 0)
	s,ms = playdate.getSecondsSinceEpoch()
	lu.assertEquals(s, 694310400)
	lu.assertEquals(ms, 0)

	playdate.fakedate.frame_pass()
	playdate.fakedate.frame_pass()
	lu.assertEquals(playdate.getCurrentTimeMilliseconds(), 200)
	lu.assertEquals(playdate.getElapsedTime(), 0.200)
	s,ms = playdate.getSecondsSinceEpoch()
	lu.assertEquals(s, 694310400)
	lu.assertEquals(ms, 200)

	playdate.resetElapsedTime()
	playdate.fakedate.frame_pass()
	lu.assertEquals(playdate.getCurrentTimeMilliseconds(), 300)
	lu.assertEquals(playdate.getElapsedTime(), 0.100)
	s,ms = playdate.getSecondsSinceEpoch()
	lu.assertEquals(s, 694310400)
	lu.assertEquals(ms, 300)

end

function test_systemInfo()
	lu.assertEquals(playdate.systemInfo.buildtime, "20000101_000000")
	lu.assertEquals(playdate.systemInfo.commit, "fakedate")
	lu.assertEquals(playdate.systemInfo.pdxcompatversion, 10000)
	lu.assertEquals(playdate.systemInfo.pdxversion, 11200)
	lu.assertEquals(playdate.systemInfo.sdk, "1.12.2")
end

function test_expected_names()
	local playdate_expected = [[
		datastore
		file.file
		geometry.affineTransform
		geometry.arc
		geometry.lineSegment
		geometry.point
		geometry.polygon
		geometry.rect
		geometry.size
		geometry.vector2D
		graphics.font
		graphics.image
		graphics.imagetable
		graphics.sprite.collisioninfo
		graphics.sprite
		graphics.tilemap
		graphics.video
		inputHandlers
		menu
		menu.item
		pathfinder.graph
		pathfinder.node
		simulator
		sound.bitcrusher
		sound.channel
		sound.controlsignal
		sound.delayline
		sound.delaylinetap
		sound.envelope
		sound.fileplayer
		sound.instrument
		sound.lfo
		sound.onepolefilter
		sound.overdrive
		sound.ringmod
		sound.sample
		sound.sampleplayer
		sound.sequence
		sound.signal
		sound.synth
		sound.track
		sound.twopolefilter
		]]
	for name in string.gmatch(playdate_expected, "(%S+)") do
		local fullname = "playdate."..name
		local root = playdate
		for part in string.gmatch(name, "([^.]+)") do
			lu.assertNotNil(root[part])
			root = root[part]
		end
		lu.assertEquals(root.__name, fullname)
	end
end


local considered = 0
function recurse_test_names(node, path)
	considered = considered + 1
	if type(node) ~= "table" then
		--print(path.." non-table")
		return
	end
	if node.__name ~= nil then
		assert(node.__name == path, string.format("name doesn't match:\nnode: %s\npath: %s", node.__name, path))
	end
	for k,v in pairs(node) do
		--print(path,k,v)
		if k ~= "__index" then
			recurse_test_names(v, path.."."..k)
		end
	end
end

function test_correct_names()
	recurse_test_names(json, "json")
	recurse_test_names(playdate, "playdate")
end

function test_not_implemented()
	lu.assertErrorMsgContains("playdate.fakedate.not_implemented is not yet implemented", playdate.fakedate.not_implemented)
end



--------------------------------------------------------------------------------
--
-- main
--
os.exit( lu.LuaUnit.run() )
