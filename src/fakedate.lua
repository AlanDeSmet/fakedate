-- This file is part of fakedate, an alternate Playdate SDK for testing.
-- Copyright 2022 Alan De Smet
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

if import ~= nil then
	return
end

--package.path = os.getenv("PLAYDATE_SDK_PATH").."/CoreLibs/?.lua"
--require "object" -- Doesn't work, relies on Playdate += extension

require "nocommitobject"

_G["import"] = require



function assertValidArgObject(funcname, argnum, expectedname, obj)
	local function errmsg(thetype)
		return string.format("bad argument #%d to '%s' (%s expected, got %s)", argnum, funcname, expectedname, thetype)
	end
	assert(type(obj) == "table", errmsg(type(obj)))
	assert(obj.__name ~= nil, errmsg("non-object table"))
	assert(obj.__name == expectedname, errmsg(obj.__name))
end

function PlaydateObject(className)
	local root = _G
	local path_so_far = ""
	for part in string.gmatch(className, "([^.]+)") do
		if string.len(path_so_far) > 0 then
			path_so_far = path_so_far.."."
		end
			path_so_far = path_so_far..part
		if root[part] == nil then
			root[part] = {
				__name=path_so_far
			}
		end
		root = root[part]
	end
	if root.__name == nil then
		root.__name=path_so_far
	end

	function root.__index(table, key)
		if root[key] ~= nil then
			return root[key]
		end
		if root.__realindex ~= nil then
			return root.__realindex(table, key)
		end
		return nil
	end

	function root.new(...)
		local newobj = {}
		assert(root.init ~= nil, className..".init function is missing")
		root.init(newobj, ...)
		setmetatable(newobj, root)
		return newobj
	end
end


-- _VERSION:	Lua 5.4
-- playdate.apiVersion():	11200	10000
-- Stub non-standard tables
json = {
  null = {},
  }
playdate = {
  isSimulator = 1,

  kButtonLeft  =  1,
  kButtonRight =  2,
  kButtonUp    =  4,
  kButtonDown  =  8,
  kButtonB     = 16,
  kButtonA     = 32,
  argv = {},
  datastore = {},
  display = {},
  file = {
    kSeekSet     = 0,
    kSeekFromCurrent = 1,
    kSeekFromEnd = 2,

    kFileRead    = 3,
    kFileWrite   = 4,
    kFileAppend  = 8,
    file = {},
    },
  geometry = {
    kUnflipped = 0,
    kFlippedX  = 1,
    kFlippedY  = 2,
    kFlippedXY = 3,
  },
  graphics = {
    kColorBlack = 0,
    kColorWhite = 1,
    kColorClear = 2,
    kColorXOR = 3,

    kDrawModeCopy = 0,
    kDrawModeWhiteTransparent = 1,
    kDrawModeBlackTransparent = 2,
    kDrawModeFillWhite = 3,
    kDrawModeFillBlack = 4,
    kDrawModeXOR = 5,
    kDrawModeNXOR = 6,
    kDrawModeInverted = 7,

    kImageUnflipped = 0,
    kImageFlippedX = 1,
    kImageFlippedY = 2,
    kImageFlippedXY = 3,

    kLineCapStyleButt = 0,
    kLineCapStyleSquare = 1,
    kLineCapStyleRound = 2,

    kPolygonFillNonZero = 0,
    kPolygonFillEvenOdd = 1,

    kStrokeCentered = 0,
    kStrokeInside = 1,
    kStrokeOutside = 2,

    font = {
      kLanguageEnglish  = 0,
      kLanguageJapanese = 1,

      kVariantNormal = 0,
      kVariantBold   = 1,
      kVariantItalic = 2,
	},
    image = {
      kDitherTypeNone           =  0,
      kDitherTypeDiagonalLine   =  1,
      kDitherTypeVerticalLine   =  2,
      kDitherTypeHorizontalLine =  3,
      kDitherTypeScreen         =  4,
      kDitherTypeBayer2x2       =  5,
      kDitherTypeBayer4x4       =  6,
      kDitherTypeBayer8x8       =  7,
      kDitherTypeFloydSteinberg =  8,
      kDitherTypeBurkes         =  9,
      kDitherTypeAtkinson       = 10,
	},
    imagetable = {},
    sprite = {
      kCollisionTypeSlide = 0,
      kCollisionTypeFreeze = 1,
      kCollisionTypeOverlap = 2,
      kCollisionTypeBounce = 3,
      collisioninfo = {},
      },
    tilemap = {},
    video = {},
    },
  inputHandlers = {
    },
  menu = {
    item = {},
    },
  metadata = {},
  pathfinder = {
    node = {},
    },
  scoreboards = {},
  server = {},
  simulator = {},
  sound = {
    kFilterLowPass   = 0,
    kFilterHighPass  = 1,
    kFilterBandPass  = 2,
    kFilterNotch     = 3,
    kFilterPEQ       = 4,
    kFilterLowShelf  = 5,
    kFilterHighShelf = 6,

    kFormat8bitMono    = 0,
    kFormat8bitStereo  = 1,
    kFormat16bitMono   = 2,
    kFormat16bitStereo = 3,

    kLFOSquare        = 0,
    kLFOTriangle      = 1,
    kLFOSine          = 2,
    kLFOSampleAndHold = 3,
    kLFOSawtoothUp    = 4,
    kLFOSawtoothDown  = 5,

    kWaveSquare    = 0,
    kWaveTriangle  = 1,
    kWaveSine      = 2,
    kWaveNoise     = 3,
    kWaveSawtooth  = 4,
    kWavePOPhase   = 5,
    kWavePODigital = 6,
    kWavePOVosim   = 7,

    delaylinetap = {},
    micinput = {},
    signal = {},
    },
  }

_fakedate_not_implemented = {}

local function make_not_implemented(function_name)
	_fakedate_not_implemented[function_name] = true
	return function()
		assert(false, function_name.." is not yet implemented")
	end
end

--------------------------------------------------------------------------------
--
-- json
--
json.decode = make_not_implemented("json.decode")
json.decodeFile = make_not_implemented("json.decodeFile")
json.encode = make_not_implemented("json.encode")
json.encodePretty = make_not_implemented("json.encodePretty")
json.encodeToFile = make_not_implemented("json.encodeToFile")
json.null.__call = make_not_implemented("json.null.__call")
json.null.__tostring = make_not_implemented("json.null.__tostring")

--------------------------------------------------------------------------------
--
-- playdate misc
--
playdate.GMTTimeFromEpoch = make_not_implemented("playdate.GMTTimeFromEpoch")
playdate.accelerometerIsRunning = make_not_implemented("playdate.accelerometerIsRunning")
playdate.apiVersion = make_not_implemented("playdate.apiVersion")
playdate.buttonIsPressed = make_not_implemented("playdate.buttonIsPressed")
playdate.buttonJustPressed = make_not_implemented("playdate.buttonJustPressed")
playdate.buttonJustReleased = make_not_implemented("playdate.buttonJustReleased")
playdate.clearConsole = make_not_implemented("playdate.clearConsole")
playdate.drawFPS = make_not_implemented("playdate.drawFPS")
playdate.epochFromGMTTime = make_not_implemented("playdate.epochFromGMTTime")
playdate.epochFromTime = make_not_implemented("playdate.epochFromTime")
--playdate.inputHandlers.1 = nil
playdate.inputHandlers.__name = "playdate.inputHandlers"
playdate.inputHandlers.pop = make_not_implemented("playdate.inputHandlers.pop")
playdate.inputHandlers.push = make_not_implemented("playdate.inputHandlers.push")
playdate.isCrankDocked = make_not_implemented("playdate.isCrankDocked")
playdate.readAccelerometer = make_not_implemented("playdate.readAccelerometer")
playdate.setAutoLockDisabled = make_not_implemented("playdate.setAutoLockDisabled")
playdate.setCollectsGarbage = make_not_implemented("playdate.setCollectsGarbage")
playdate.setCrankSoundsDisabled = make_not_implemented("playdate.setCrankSoundsDisabled")
playdate.setDebugDrawColor = make_not_implemented("playdate.setDebugDrawColor")
playdate.setGCScaling = make_not_implemented("playdate.setGCScaling")
playdate.setMenuImage = make_not_implemented("playdate.setMenuImage")
playdate.setMinimumGCTime = make_not_implemented("playdate.setMinimumGCTime")
playdate.setNewlinePrinted = make_not_implemented("playdate.setNewlinePrinted")
playdate.setStatsInterval = make_not_implemented("playdate.setStatsInterval")
playdate.start = make_not_implemented("playdate.start")
playdate.startAccelerometer = make_not_implemented("playdate.startAccelerometer")
playdate.stop = make_not_implemented("playdate.stop")
playdate.stopAccelerometer = make_not_implemented("playdate.stopAccelerometer")
playdate.timeFromEpoch = make_not_implemented("playdate.timeFromEpoch")
playdate.update = make_not_implemented("playdate.update")
playdate.wait = make_not_implemented("playdate.wait")

--------------------------------------------------------------------------------
--
-- playdate.datastore
--

playdate.datastore.__name = "playdate.datastore"
playdate.datastore.delete = make_not_implemented("playdate.datastore.delete")
playdate.datastore.read = make_not_implemented("playdate.datastore.read")
playdate.datastore.readImage = make_not_implemented("playdate.datastore.readImage")
playdate.datastore.write = make_not_implemented("playdate.datastore.write")
playdate.datastore.writeImage = make_not_implemented("playdate.datastore.writeImage")

--------------------------------------------------------------------------------
--
-- playdate.display
--
playdate.display.flush = make_not_implemented("playdate.display.flush")
function playdate.display.getHeight()
	return 240/playdate.display.getScale()
end
playdate.display.getInverted = make_not_implemented("playdate.display.getInverted")
playdate.display.getMosaic = make_not_implemented("playdate.display.getMosaic")
playdate.display.getOffset = make_not_implemented("playdate.display.getOffset")
function playdate.display.getRect()
	return playdate.geometry.rect.new(0, 0, playdate.display.getWidth(), playdate.display.getHeight())
end
playdate.display.getRefreshRate = make_not_implemented("playdate.display.getRefreshRate")

function playdate.display.getScale()
	return playdate.fakedate.display.scale
end

function playdate.display.getSize()
	return playdate.display.getWidth(),playdate.display.getHeight()
end
function playdate.display.getWidth()
	return 400/playdate.display.getScale()
end
playdate.display.loadImage = make_not_implemented("playdate.display.loadImage")
playdate.display.setFlipped = make_not_implemented("playdate.display.setFlipped")
playdate.display.setInverted = make_not_implemented("playdate.display.setInverted")
playdate.display.setMosaic = make_not_implemented("playdate.display.setMosaic")
playdate.display.setOffset = make_not_implemented("playdate.display.setOffset")

function playdate.display.setRefreshRate(fps)
	if fps > 50 then
		fps = 50
	end
	if fps <= 0 then
		-- 0 really means "as fast as possible", but
		-- since we're faking everything, that's nonsense.
		fps = 50
	end
	playdate.fakedate.refreshRate = fps
end

function playdate.display.setScale(scale)
	-- PlaydateSimulator assigns 1 or 8 depending on 
	-- exactly how silly you scale <1 is, but let's ignore
	-- it as silly.
	assert(scale >= 1)
	if scale > 8 then
		scale = 8
	end
	playdate.fakedate.display.scale = 2^(math.floor(math.log(scale,2)))
end


--------------------------------------------------------------------------------
--
-- playdate.file
--
playdate.file.delete = make_not_implemented("playdate.file.delete")
playdate.file.exists = make_not_implemented("playdate.file.exists")
playdate.file.file.__gc = make_not_implemented("playdate.file.file.__gc")
playdate.file.file.__name = "playdate.file.file"
playdate.file.file.close = make_not_implemented("playdate.file.file.close")
playdate.file.file.flush = make_not_implemented("playdate.file.file.flush")
playdate.file.file.read = make_not_implemented("playdate.file.file.read")
playdate.file.file.readline = make_not_implemented("playdate.file.file.readline")
playdate.file.file.seek = make_not_implemented("playdate.file.file.seek")
playdate.file.file.tell = make_not_implemented("playdate.file.file.tell")
playdate.file.file.write = make_not_implemented("playdate.file.file.write")
playdate.file.getSize = make_not_implemented("playdate.file.getSize")
playdate.file.getType = make_not_implemented("playdate.file.getType")
playdate.file.isdir = make_not_implemented("playdate.file.isdir")
playdate.file.listFiles = make_not_implemented("playdate.file.listFiles")
playdate.file.load = make_not_implemented("playdate.file.load")
playdate.file.mkdir = make_not_implemented("playdate.file.mkdir")
playdate.file.modtime = make_not_implemented("playdate.file.modtime")
playdate.file.open = make_not_implemented("playdate.file.open")
playdate.file.rename = make_not_implemented("playdate.file.rename")
playdate.file.run = make_not_implemented("playdate.file.run")

--------------------------------------------------------------------------------
--
-- playdate.geometry misc
--
playdate.geometry.squaredDistanceToPoint = make_not_implemented("playdate.geometry.squaredDistanceToPoint")
playdate.geometry.distanceToPoint = make_not_implemented("playdate.geometry.distanceToPoint")

--------------------------------------------------------------------------------
--
-- playdate.geometry.affineTransform
--

PlaydateObject("playdate.geometry.affineTransform")
playdate.geometry.affineTransform.__eq = make_not_implemented("playdate.geometry.affineTransform.__eq")
playdate.geometry.affineTransform.__mul = make_not_implemented("playdate.geometry.affineTransform.__mul")
playdate.geometry.affineTransform.__tostring = make_not_implemented("playdate.geometry.affineTransform.__tostring")
playdate.geometry.affineTransform.concat = make_not_implemented("playdate.geometry.affineTransform.concat")
playdate.geometry.affineTransform.copy = make_not_implemented("playdate.geometry.affineTransform.copy")
playdate.geometry.affineTransform.invert = make_not_implemented("playdate.geometry.affineTransform.invert")
playdate.geometry.affineTransform.reset = make_not_implemented("playdate.geometry.affineTransform.reset")
playdate.geometry.affineTransform.rotate = make_not_implemented("playdate.geometry.affineTransform.rotate")
playdate.geometry.affineTransform.rotatedBy = make_not_implemented("playdate.geometry.affineTransform.rotatedBy")
playdate.geometry.affineTransform.scale = make_not_implemented("playdate.geometry.affineTransform.scale")
playdate.geometry.affineTransform.scaledBy = make_not_implemented("playdate.geometry.affineTransform.scaledBy")
playdate.geometry.affineTransform.skew = make_not_implemented("playdate.geometry.affineTransform.skew")
playdate.geometry.affineTransform.skewedBy = make_not_implemented("playdate.geometry.affineTransform.skewedBy")
playdate.geometry.affineTransform.transformAABB = make_not_implemented("playdate.geometry.affineTransform.transformAABB")
playdate.geometry.affineTransform.transformLineSegment = make_not_implemented("playdate.geometry.affineTransform.transformLineSegment")
playdate.geometry.affineTransform.transformPoint = make_not_implemented("playdate.geometry.affineTransform.transformPoint")
playdate.geometry.affineTransform.transformPolygon = make_not_implemented("playdate.geometry.affineTransform.transformPolygon")
playdate.geometry.affineTransform.transformXY = make_not_implemented("playdate.geometry.affineTransform.transformXY")
playdate.geometry.affineTransform.transformedAABB = make_not_implemented("playdate.geometry.affineTransform.transformedAABB")
playdate.geometry.affineTransform.transformedLineSegment = make_not_implemented("playdate.geometry.affineTransform.transformedLineSegment")
playdate.geometry.affineTransform.transformedPoint = make_not_implemented("playdate.geometry.affineTransform.transformedPoint")
playdate.geometry.affineTransform.transformedPolygon = make_not_implemented("playdate.geometry.affineTransform.transformedPolygon")
playdate.geometry.affineTransform.translate = make_not_implemented("playdate.geometry.affineTransform.translate")
playdate.geometry.affineTransform.translatedBy = make_not_implemented("playdate.geometry.affineTransform.translatedBy")

--------------------------------------------------------------------------------
--
-- playdate.geometry.arc
--
PlaydateObject("playdate.geometry.arc")
playdate.geometry.arc.__eq = make_not_implemented("playdate.geometry.arc.__eq")
--playdate.geometry.arc.__index = not_implemented
playdate.geometry.arc.__newindex = make_not_implemented("playdate.geometry.arc.__newindex")
playdate.geometry.arc.__tostring = make_not_implemented("playdate.geometry.arc.__tostring")
playdate.geometry.arc.copy = make_not_implemented("playdate.geometry.arc.copy")
playdate.geometry.arc.isClockwise = make_not_implemented("playdate.geometry.arc.isClockwise")
playdate.geometry.arc.length = make_not_implemented("playdate.geometry.arc.length")
playdate.geometry.arc.pointOnArc = make_not_implemented("playdate.geometry.arc.pointOnArc")
playdate.geometry.arc.setIsClockwise = make_not_implemented("playdate.geometry.arc.setIsClockwise")

--------------------------------------------------------------------------------
--
-- playdate.geometry.lineSegment
--
PlaydateObject("playdate.geometry.lineSegment")
playdate.geometry.lineSegment.__eq = make_not_implemented("playdate.geometry.lineSegment.__eq")
--playdate.geometry.lineSegment.__index = not_implemented
playdate.geometry.lineSegment.__newindex = make_not_implemented("playdate.geometry.lineSegment.__newindex")
playdate.geometry.lineSegment.__tostring = make_not_implemented("playdate.geometry.lineSegment.__tostring")
playdate.geometry.lineSegment.closestPointOnLineToPoint = make_not_implemented("playdate.geometry.lineSegment.closestPointOnLineToPoint")
playdate.geometry.lineSegment.copy = make_not_implemented("playdate.geometry.lineSegment.copy")
playdate.geometry.lineSegment.fast_intersection = make_not_implemented("playdate.geometry.lineSegment.fast_intersection")
playdate.geometry.lineSegment.intersectsLineSegment = make_not_implemented("playdate.geometry.lineSegment.intersectsLineSegment")
playdate.geometry.lineSegment.intersectsPolygon = make_not_implemented("playdate.geometry.lineSegment.intersectsPolygon")
playdate.geometry.lineSegment.intersectsRect = make_not_implemented("playdate.geometry.lineSegment.intersectsRect")
playdate.geometry.lineSegment.length = make_not_implemented("playdate.geometry.lineSegment.length")
playdate.geometry.lineSegment.midPoint = make_not_implemented("playdate.geometry.lineSegment.midPoint")
playdate.geometry.lineSegment.offset = make_not_implemented("playdate.geometry.lineSegment.offset")
playdate.geometry.lineSegment.offsetBy = make_not_implemented("playdate.geometry.lineSegment.offsetBy")
playdate.geometry.lineSegment.pointOnLine = make_not_implemented("playdate.geometry.lineSegment.pointOnLine")
playdate.geometry.lineSegment.segmentVector = make_not_implemented("playdate.geometry.lineSegment.segmentVector")
playdate.geometry.lineSegment.unpack = make_not_implemented("playdate.geometry.lineSegment.unpack")

--------------------------------------------------------------------------------
--
-- playdate.geometry.point
--
--

PlaydateObject("playdate.geometry.point")

function playdate.geometry.point:__add(vec)
	assertValidArgObject("add", 2, "playdate.geometry.vector2D", vec)
	return playdate.geometry.point.new(self.x+vec.dx, self.y+vec.dy)
end

function playdate.geometry.point:__sub(vec)
	errmsg = "The right-hand argument to playdate.geometry.point's - operator should be another point (returning a vector), or a playdate.geometry.vector2D"
	assert(type(vec) == "table", errmsg)
	if vec.__name == "playdate.geometry.vector2D" then
		return playdate.geometry.point.new(self.x-vec.dx, self.y-vec.dy)
	end
	if vec.__name == "playdate.geometry.point" then
		return playdate.geometry.vector2D.new(self.x-vec.x, self.y-vec.y)
	end
	assert(false, errmsg)
end

function playdate.geometry.point:__eq(pt)
	assertValidArgObject("eq", 2, "playdate.geometry.point", pt)
	return self.x == pt.x and self.y == pt.y
end

function playdate.geometry.point.__tostring(pt)
	return "("..tostring(pt.x)..", "..tostring(pt.y)..")"
end

playdate.geometry.point.__concat = make_not_implemented("playdate.geometry.point.__concat")
--playdate.geometry.point.__index = not_implemented
playdate.geometry.point.__mul = make_not_implemented("playdate.geometry.point.__mul")
playdate.geometry.point.__newindex = make_not_implemented("playdate.geometry.point.__newindex")

function playdate.geometry.point:copy()
	return  playdate.geometry.point.new(self.x,self.y)
end

function playdate.geometry.point:distanceToPoint(pt2)
	return self:squaredDistanceToPoint(pt2)^0.5
end

function playdate.geometry.point.new(x,y)
	local ret = {x=x, y=y}
	setmetatable(ret, playdate.geometry.point)
	return ret
end

function playdate.geometry.point:offset(dx,dy)
	self.x = self.x + dx
	self.y = self.y + dy
	return self
end

function playdate.geometry.point:offsetBy(dx,dy)
	return playdate.geometry.point.new(self.x+dx, self.y+dy)
end

function playdate.geometry.point:squaredDistanceToPoint(pt2)
	return (self.x-pt2.x)^2 + (self.y-pt2.y)^2
end

playdate.geometry.point.unpack = make_not_implemented("playdate.geometry.point.unpack")

--------------------------------------------------------------------------------
--
-- playdate.geometry.polygon
--
PlaydateObject("playdate.geometry.polygon")
playdate.geometry.polygon.__eq = make_not_implemented("playdate.geometry.polygon.__eq")
playdate.geometry.polygon.__mul = make_not_implemented("playdate.geometry.polygon.__mul")
playdate.geometry.polygon.__tostring = make_not_implemented("playdate.geometry.polygon.__tostring")
playdate.geometry.polygon.close = make_not_implemented("playdate.geometry.polygon.close")
playdate.geometry.polygon.containsPoint = make_not_implemented("playdate.geometry.polygon.containsPoint")
playdate.geometry.polygon.copy = make_not_implemented("playdate.geometry.polygon.copy")
playdate.geometry.polygon.count = make_not_implemented("playdate.geometry.polygon.count")
playdate.geometry.polygon.getBounds = make_not_implemented("playdate.geometry.polygon.getBounds")
playdate.geometry.polygon.getBoundsRect = make_not_implemented("playdate.geometry.polygon.getBoundsRect")
playdate.geometry.polygon.getPointAt = make_not_implemented("playdate.geometry.polygon.getPointAt")
playdate.geometry.polygon.intersects = make_not_implemented("playdate.geometry.polygon.intersects")
playdate.geometry.polygon.isClosed = make_not_implemented("playdate.geometry.polygon.isClosed")
playdate.geometry.polygon.length = make_not_implemented("playdate.geometry.polygon.length")
playdate.geometry.polygon.pointOnPolygon = make_not_implemented("playdate.geometry.polygon.pointOnPolygon")
playdate.geometry.polygon.setPointAt = make_not_implemented("playdate.geometry.polygon.setPointAt")
playdate.geometry.polygon.translate = make_not_implemented("playdate.geometry.polygon.translate")

--------------------------------------------------------------------------------
--
-- playdate.geometry.rect
--
PlaydateObject("playdate.geometry.rect")
function playdate.geometry.rect:__eq(other)
	assertValidArgObject("eq", 2, "playdate.geometry.rect", other)
	return (self.x == other.x and
		self.y == other.y and
		self.width == other.width and
		self.height == other.height)
end
playdate.geometry.rect.__tostring = make_not_implemented("playdate.geometry.rect.__tostring")
playdate.geometry.rect.centerPoint = make_not_implemented("playdate.geometry.rect.centerPoint")
playdate.geometry.rect.containsPoint = make_not_implemented("playdate.geometry.rect.containsPoint")
playdate.geometry.rect.containsRect = make_not_implemented("playdate.geometry.rect.containsRect")
playdate.geometry.rect.copy = make_not_implemented("playdate.geometry.rect.copy")
playdate.geometry.rect.fast_intersection = make_not_implemented("playdate.geometry.rect.fast_intersection")
playdate.geometry.rect.fast_union = make_not_implemented("playdate.geometry.rect.fast_union")
playdate.geometry.rect.flipRelativeToRect = make_not_implemented("playdate.geometry.rect.flipRelativeToRect")
playdate.geometry.rect.inset = make_not_implemented("playdate.geometry.rect.inset")
playdate.geometry.rect.insetBy = make_not_implemented("playdate.geometry.rect.insetBy")
playdate.geometry.rect.intersection = make_not_implemented("playdate.geometry.rect.intersection")
playdate.geometry.rect.intersects = make_not_implemented("playdate.geometry.rect.intersects")
playdate.geometry.rect.isEmpty = make_not_implemented("playdate.geometry.rect.isEmpty")
playdate.geometry.rect.isEqual = make_not_implemented("playdate.geometry.rect.isEqual")

function playdate.geometry.rect:init(x, y, width, height)
	self.x=x
	self.y=y
	self.width=width
	self.height=height
end

function playdate.geometry.rect.__realindex(table, index)
	if index == "left" then return table.x end
	if index == "top" then return table.y end
	if index == "right" then return table.x + table.width end
	if index == "bottom" then return table.y + table.height end
end

function playdate.geometry.rect.__newindex(table, index, value)
	error("Could not set value for '"..index.."' on "..getmetatable(table).__name)
end

playdate.geometry.rect.offset = make_not_implemented("playdate.geometry.rect.offset")
playdate.geometry.rect.offsetBy = make_not_implemented("playdate.geometry.rect.offsetBy")
playdate.geometry.rect.toPolygon = make_not_implemented("playdate.geometry.rect.toPolygon")
playdate.geometry.rect.union = make_not_implemented("playdate.geometry.rect.union")
playdate.geometry.rect.unpack = make_not_implemented("playdate.geometry.rect.unpack")

--------------------------------------------------------------------------------
--
-- playdate.geometry.size
--
PlaydateObject("playdate.geometry.size")
playdate.geometry.size.__eq = make_not_implemented("playdate.geometry.size.__eq")
--playdate.geometry.size.__index = not_implemented
playdate.geometry.size.__newindex = make_not_implemented("playdate.geometry.size.__newindex")
playdate.geometry.size.__tostring = make_not_implemented("playdate.geometry.size.__tostring")
playdate.geometry.size.copy = make_not_implemented("playdate.geometry.size.copy")
playdate.geometry.size.unpack = make_not_implemented("playdate.geometry.size.unpack")

--------------------------------------------------------------------------------
--
-- playdate.geometry.vector2D
--

PlaydateObject("playdate.geometry.vector2D")

function playdate.geometry.vector2D.__realindex(table, index)
	if index == "x" then return table.dx end
	if index == "y" then return table.dy end
end

function playdate.geometry.vector2D.__newindex(table, index, value)
	if index == "x" then table.dx = value end
	if index == "y" then table.dy = value end
end

function playdate.geometry.vector2D:init(dx,dy)
	self.dx = dx
	self.dy = dy
end

function playdate.geometry.vector2D.__tostring(vec)
	return "("..tostring(vec.dx)..", "..tostring(vec.dy)..")"
end

function playdate.geometry.vector2D:__eq(other)
	assertValidArgObject("eq", 2, "playdate.geometry.vector2D", other)
	return self.dx == other.dx and self.dy == other.dy
end

playdate.geometry.vector2D.__add = make_not_implemented("playdate.geometry.vector2D.__add")
playdate.geometry.vector2D.__div = make_not_implemented("playdate.geometry.vector2D.__div")
playdate.geometry.vector2D.__mul = make_not_implemented("playdate.geometry.vector2D.__mul")
playdate.geometry.vector2D.__sub = make_not_implemented("playdate.geometry.vector2D.__sub")
playdate.geometry.vector2D.__unm = make_not_implemented("playdate.geometry.vector2D.__unm")
playdate.geometry.vector2D.addVector = make_not_implemented("playdate.geometry.vector2D.addVector")
playdate.geometry.vector2D.angleBetween = make_not_implemented("playdate.geometry.vector2D.angleBetween")
playdate.geometry.vector2D.copy = make_not_implemented("playdate.geometry.vector2D.copy")
playdate.geometry.vector2D.dotProduct = make_not_implemented("playdate.geometry.vector2D.dotProduct")
playdate.geometry.vector2D.leftNormal = make_not_implemented("playdate.geometry.vector2D.leftNormal")
playdate.geometry.vector2D.magnitude = make_not_implemented("playdate.geometry.vector2D.magnitude")
playdate.geometry.vector2D.magnitudeSquared = make_not_implemented("playdate.geometry.vector2D.magnitudeSquared")
playdate.geometry.vector2D.normalize = make_not_implemented("playdate.geometry.vector2D.normalize")
playdate.geometry.vector2D.normalized = make_not_implemented("playdate.geometry.vector2D.normalized")
playdate.geometry.vector2D.projectAlong = make_not_implemented("playdate.geometry.vector2D.projectAlong")
playdate.geometry.vector2D.projectedAlong = make_not_implemented("playdate.geometry.vector2D.projectedAlong")
playdate.geometry.vector2D.rightNormal = make_not_implemented("playdate.geometry.vector2D.rightNormal")
playdate.geometry.vector2D.scale = make_not_implemented("playdate.geometry.vector2D.scale")
playdate.geometry.vector2D.scaledBy = make_not_implemented("playdate.geometry.vector2D.scaledBy")
playdate.geometry.vector2D.unpack = make_not_implemented("playdate.geometry.vector2D.unpack")

--------------------------------------------------------------------------------
--
-- playdate misc
--
playdate.getBatteryPercentage = make_not_implemented("playdate.getBatteryPercentage")
playdate.getBatteryVoltage = make_not_implemented("playdate.getBatteryVoltage")
playdate.getButtonState = make_not_implemented("playdate.getButtonState")
playdate.getCrankChange = make_not_implemented("playdate.getCrankChange")
playdate.getCrankPosition = make_not_implemented("playdate.getCrankPosition")

function playdate.getCurrentTimeMilliseconds()
	return playdate.fakedate.currentTimeMilliseconds
end

function playdate.getElapsedTime()
	return (playdate.getCurrentTimeMilliseconds() - playdate.fakedate.elapsedTimeEpoch) / 1000
end

function playdate.resetElapsedTime()
	playdate.fakedate.elapsedTimeEpoch = playdate.fakedate.currentTimeMilliseconds 
end

playdate.getFlipped = make_not_implemented("playdate.getFlipped")
playdate.getGMTTime = make_not_implemented("playdate.getGMTTime")
playdate.getPowerStatus = make_not_implemented("playdate.getPowerStatus")
playdate.getReduceFlashing = make_not_implemented("playdate.getReduceFlashing")

function playdate.getSecondsSinceEpoch()
	local cur_ms = playdate.getCurrentTimeMilliseconds()
	local epoch_seconds_to_jan_1_2022 = 694310400.0
	local seconds = math.floor(cur_ms/1000)
	local milliseconds = cur_ms - (seconds*1000)
	return seconds+epoch_seconds_to_jan_1_2022, milliseconds
end

playdate.getStats = make_not_implemented("playdate.getStats")
playdate.getSystemLanguage = make_not_implemented("playdate.getSystemLanguage")
playdate.getSystemMenu = make_not_implemented("playdate.getSystemMenu")
playdate.getTime = make_not_implemented("playdate.getTime")

--------------------------------------------------------------------------------
--
-- playdate.graphics
--
playdate.graphics.checkAlphaCollision = make_not_implemented("playdate.graphics.checkAlphaCollision")
playdate.graphics.clear = make_not_implemented("playdate.graphics.clear")
playdate.graphics.clearClipRect = make_not_implemented("playdate.graphics.clearClipRect")
playdate.graphics.clearStencil = make_not_implemented("playdate.graphics.clearStencil")
playdate.graphics.clearStencilImage = make_not_implemented("playdate.graphics.clearStencilImage")
playdate.graphics.drawEllipseInRect = make_not_implemented("playdate.graphics.drawEllipseInRect")
playdate.graphics.drawLine = make_not_implemented("playdate.graphics.drawLine")
playdate.graphics.drawLocalizedText = make_not_implemented("playdate.graphics.drawLocalizedText")
playdate.graphics.drawPixel = make_not_implemented("playdate.graphics.drawPixel")
playdate.graphics.drawPolygon = make_not_implemented("playdate.graphics.drawPolygon")
playdate.graphics.drawRect = make_not_implemented("playdate.graphics.drawRect")
playdate.graphics.drawRoundRect = make_not_implemented("playdate.graphics.drawRoundRect")
playdate.graphics.drawText = make_not_implemented("playdate.graphics.drawText")
playdate.graphics.drawTriangle = make_not_implemented("playdate.graphics.drawTriangle")
playdate.graphics.fillEllipseInRect = make_not_implemented("playdate.graphics.fillEllipseInRect")
playdate.graphics.fillPolygon = make_not_implemented("playdate.graphics.fillPolygon")
playdate.graphics.fillRect = make_not_implemented("playdate.graphics.fillRect")
playdate.graphics.fillRoundRect = make_not_implemented("playdate.graphics.fillRoundRect")
playdate.graphics.fillTriangle = make_not_implemented("playdate.graphics.fillTriangle")
playdate.graphics.getBackgroundColor = make_not_implemented("playdate.graphics.getBackgroundColor")
playdate.graphics.getClipRect = make_not_implemented("playdate.graphics.getClipRect")
playdate.graphics.getColor = make_not_implemented("playdate.graphics.getColor")
playdate.graphics.getDisplayImage = make_not_implemented("playdate.graphics.getDisplayImage")
playdate.graphics.getDrawOffset = make_not_implemented("playdate.graphics.getDrawOffset")
playdate.graphics.getFont = make_not_implemented("playdate.graphics.getFont")
playdate.graphics.getFontTracking = make_not_implemented("playdate.graphics.getFontTracking")
playdate.graphics.getImageDrawMode = make_not_implemented("playdate.graphics.getImageDrawMode")
playdate.graphics.getLineWidth = make_not_implemented("playdate.graphics.getLineWidth")
playdate.graphics.getLocalizedText = make_not_implemented("playdate.graphics.getLocalizedText")
playdate.graphics.getScreenClipRect = make_not_implemented("playdate.graphics.getScreenClipRect")
playdate.graphics.getStrokeLocation = make_not_implemented("playdate.graphics.getStrokeLocation")
playdate.graphics.getSystemFont = make_not_implemented("playdate.graphics.getSystemFont")
playdate.graphics.getTextSize = make_not_implemented("playdate.graphics.getTextSize")
playdate.graphics.getWorkingImage = make_not_implemented("playdate.graphics.getWorkingImage")
playdate.graphics.imageSizeAtPath = make_not_implemented("playdate.graphics.imageSizeAtPath")
playdate.graphics.lockFocus = make_not_implemented("playdate.graphics.lockFocus")
playdate.graphics.perlin = make_not_implemented("playdate.graphics.perlin")
playdate.graphics.perlinArray = make_not_implemented("playdate.graphics.perlinArray")
playdate.graphics.popContext = make_not_implemented("playdate.graphics.popContext")
playdate.graphics.pushContext = make_not_implemented("playdate.graphics.pushContext")
playdate.graphics.setBackgroundColor = make_not_implemented("playdate.graphics.setBackgroundColor")
playdate.graphics.setClipRect = make_not_implemented("playdate.graphics.setClipRect")
playdate.graphics.setColor = make_not_implemented("playdate.graphics.setColor")
playdate.graphics.setDitherPattern = make_not_implemented("playdate.graphics.setDitherPattern")
playdate.graphics.setDrawOffset = make_not_implemented("playdate.graphics.setDrawOffset")
playdate.graphics.setFont = make_not_implemented("playdate.graphics.setFont")
playdate.graphics.setFontFamily = make_not_implemented("playdate.graphics.setFontFamily")
playdate.graphics.setFontTracking = make_not_implemented("playdate.graphics.setFontTracking")
playdate.graphics.setImageDrawMode = make_not_implemented("playdate.graphics.setImageDrawMode")
playdate.graphics.setLineCapStyle = make_not_implemented("playdate.graphics.setLineCapStyle")
playdate.graphics.setLineWidth = make_not_implemented("playdate.graphics.setLineWidth")
playdate.graphics.setPattern = make_not_implemented("playdate.graphics.setPattern")
playdate.graphics.setPolygonFillRule = make_not_implemented("playdate.graphics.setPolygonFillRule")
playdate.graphics.setScreenClipRect = make_not_implemented("playdate.graphics.setScreenClipRect")
playdate.graphics.setStencilImage = make_not_implemented("playdate.graphics.setStencilImage")
playdate.graphics.setStencilPattern = make_not_implemented("playdate.graphics.setStencilPattern")
playdate.graphics.setStrokeLocation = make_not_implemented("playdate.graphics.setStrokeLocation")
playdate.graphics.unlockFocus = make_not_implemented("playdate.graphics.unlockFocus")

--------------------------------------------------------------------------------
--
-- playdate.graphics.font
--
PlaydateObject("playdate.graphics.font")
playdate.graphics.font.__gc = make_not_implemented("playdate.graphics.font.__gc")
playdate.graphics.font.drawText = make_not_implemented("playdate.graphics.font.drawText")
playdate.graphics.font.getGlyph = make_not_implemented("playdate.graphics.font.getGlyph")
playdate.graphics.font.getHeight = make_not_implemented("playdate.graphics.font.getHeight")
playdate.graphics.font.getLeading = make_not_implemented("playdate.graphics.font.getLeading")
playdate.graphics.font.getTextWidth = make_not_implemented("playdate.graphics.font.getTextWidth")
playdate.graphics.font.getTracking = make_not_implemented("playdate.graphics.font.getTracking")
playdate.graphics.font.newFamily = make_not_implemented("playdate.graphics.font.newFamily")
playdate.graphics.font.setLeading = make_not_implemented("playdate.graphics.font.setLeading")
playdate.graphics.font.setTracking = make_not_implemented("playdate.graphics.font.setTracking")

--------------------------------------------------------------------------------
--
-- playdate.graphics.image
--
PlaydateObject("playdate.graphics.image")
playdate.graphics.image.__gc = make_not_implemented("playdate.graphics.image.__gc")
--playdate.graphics.image.__index = not_implemented
playdate.graphics.image.addMask = make_not_implemented("playdate.graphics.image.addMask")
playdate.graphics.image.blendWithImage = make_not_implemented("playdate.graphics.image.blendWithImage")
playdate.graphics.image.blurredImage = make_not_implemented("playdate.graphics.image.blurredImage")
playdate.graphics.image.clear = make_not_implemented("playdate.graphics.image.clear")
playdate.graphics.image.clearMask = make_not_implemented("playdate.graphics.image.clearMask")
playdate.graphics.image.copy = make_not_implemented("playdate.graphics.image.copy")
playdate.graphics.image.draw = make_not_implemented("playdate.graphics.image.draw")
playdate.graphics.image.drawBlurred = make_not_implemented("playdate.graphics.image.drawBlurred")
playdate.graphics.image.drawFaded = make_not_implemented("playdate.graphics.image.drawFaded")
playdate.graphics.image.drawIgnoringOffset = make_not_implemented("playdate.graphics.image.drawIgnoringOffset")
playdate.graphics.image.drawRotated = make_not_implemented("playdate.graphics.image.drawRotated")
playdate.graphics.image.drawSampled = make_not_implemented("playdate.graphics.image.drawSampled")
playdate.graphics.image.drawScaled = make_not_implemented("playdate.graphics.image.drawScaled")
playdate.graphics.image.drawTiled = make_not_implemented("playdate.graphics.image.drawTiled")
playdate.graphics.image.drawWithTransform = make_not_implemented("playdate.graphics.image.drawWithTransform")
playdate.graphics.image.fadedImage = make_not_implemented("playdate.graphics.image.fadedImage")
playdate.graphics.image.getMaskImage = make_not_implemented("playdate.graphics.image.getMaskImage")
playdate.graphics.image.getSize = make_not_implemented("playdate.graphics.image.getSize")
playdate.graphics.image.hasMask = make_not_implemented("playdate.graphics.image.hasMask")
playdate.graphics.image.invertedImage = make_not_implemented("playdate.graphics.image.invertedImage")
playdate.graphics.image.load = make_not_implemented("playdate.graphics.image.load")
playdate.graphics.image.removeMask = make_not_implemented("playdate.graphics.image.removeMask")
playdate.graphics.image.rotatedImage = make_not_implemented("playdate.graphics.image.rotatedImage")
playdate.graphics.image.sample = make_not_implemented("playdate.graphics.image.sample")
playdate.graphics.image.scaledImage = make_not_implemented("playdate.graphics.image.scaledImage")
playdate.graphics.image.setInverted = make_not_implemented("playdate.graphics.image.setInverted")
playdate.graphics.image.setMaskImage = make_not_implemented("playdate.graphics.image.setMaskImage")
playdate.graphics.image.transformedImage = make_not_implemented("playdate.graphics.image.transformedImage")
playdate.graphics.image.vcrPauseFilterImage = make_not_implemented("playdate.graphics.image.vcrPauseFilterImage")

--------------------------------------------------------------------------------
--
-- playdate.graphics.imagetable
--
PlaydateObject("playdate.graphics.imagetable")
playdate.graphics.imagetable.__gc = make_not_implemented("playdate.graphics.imagetable.__gc")
--playdate.graphics.imagetable.__index = not_implemented
playdate.graphics.imagetable.__len = make_not_implemented("playdate.graphics.imagetable.__len")
playdate.graphics.imagetable.__newindex = make_not_implemented("playdate.graphics.imagetable.__newindex")
playdate.graphics.imagetable.drawImage = make_not_implemented("playdate.graphics.imagetable.drawImage")
playdate.graphics.imagetable.getImage = make_not_implemented("playdate.graphics.imagetable.getImage")
playdate.graphics.imagetable.getLength = make_not_implemented("playdate.graphics.imagetable.getLength")
playdate.graphics.imagetable.getSize = make_not_implemented("playdate.graphics.imagetable.getSize")
playdate.graphics.imagetable.load = make_not_implemented("playdate.graphics.imagetable.load")
playdate.graphics.imagetable.setImage = make_not_implemented("playdate.graphics.imagetable.setImage")

--------------------------------------------------------------------------------
--
-- playdate.graphics.sprite
--
PlaydateObject("playdate.graphics.sprite")
playdate.graphics.sprite.__gc = make_not_implemented("playdate.graphics.sprite.__gc")
playdate.graphics.sprite.add = make_not_implemented("playdate.graphics.sprite.add")
playdate.graphics.sprite.addDirtyRect = make_not_implemented("playdate.graphics.sprite.addDirtyRect")
playdate.graphics.sprite.addSprite = make_not_implemented("playdate.graphics.sprite.addSprite")
playdate.graphics.sprite.allOverlappingSprites = make_not_implemented("playdate.graphics.sprite.allOverlappingSprites")
playdate.graphics.sprite.alphaCollision = make_not_implemented("playdate.graphics.sprite.alphaCollision")
playdate.graphics.sprite.checkCollisions = make_not_implemented("playdate.graphics.sprite.checkCollisions")
playdate.graphics.sprite.clearClipRect = make_not_implemented("playdate.graphics.sprite.clearClipRect")
playdate.graphics.sprite.clearClipRectsInRange = make_not_implemented("playdate.graphics.sprite.clearClipRectsInRange")
playdate.graphics.sprite.clearCollideRect = make_not_implemented("playdate.graphics.sprite.clearCollideRect")
playdate.graphics.sprite.clearStencil = make_not_implemented("playdate.graphics.sprite.clearStencil")
playdate.graphics.sprite.collisioninfo.__gc = make_not_implemented("playdate.graphics.sprite.collisioninfo.__gc")
--playdate.graphics.sprite.collisioninfo.__index = not_implemented
playdate.graphics.sprite.collisioninfo.__name = "playdate.graphics.sprite.collisioninfo"
playdate.graphics.sprite.collisionsEnabled = make_not_implemented("playdate.graphics.sprite.collisionsEnabled")
playdate.graphics.sprite.copy = make_not_implemented("playdate.graphics.sprite.copy")
playdate.graphics.sprite.getAllSprites = make_not_implemented("playdate.graphics.sprite.getAllSprites")
playdate.graphics.sprite.getAlwaysRedraw = make_not_implemented("playdate.graphics.sprite.getAlwaysRedraw")
playdate.graphics.sprite.getBounds = make_not_implemented("playdate.graphics.sprite.getBounds")
playdate.graphics.sprite.getBoundsRect = make_not_implemented("playdate.graphics.sprite.getBoundsRect")
playdate.graphics.sprite.getCenter = make_not_implemented("playdate.graphics.sprite.getCenter")
playdate.graphics.sprite.getCenterPoint = make_not_implemented("playdate.graphics.sprite.getCenterPoint")
playdate.graphics.sprite.getCollideBounds = make_not_implemented("playdate.graphics.sprite.getCollideBounds")
playdate.graphics.sprite.getCollideRect = make_not_implemented("playdate.graphics.sprite.getCollideRect")
playdate.graphics.sprite.getCollidesWithGroupsMask = make_not_implemented("playdate.graphics.sprite.getCollidesWithGroupsMask")
playdate.graphics.sprite.getGroupMask = make_not_implemented("playdate.graphics.sprite.getGroupMask")
playdate.graphics.sprite.getImage = make_not_implemented("playdate.graphics.sprite.getImage")
playdate.graphics.sprite.getImageFlip = make_not_implemented("playdate.graphics.sprite.getImageFlip")
playdate.graphics.sprite.getPosition = make_not_implemented("playdate.graphics.sprite.getPosition")
playdate.graphics.sprite.getRotation = make_not_implemented("playdate.graphics.sprite.getRotation")
playdate.graphics.sprite.getScale = make_not_implemented("playdate.graphics.sprite.getScale")
playdate.graphics.sprite.getSize = make_not_implemented("playdate.graphics.sprite.getSize")
playdate.graphics.sprite.getTag = make_not_implemented("playdate.graphics.sprite.getTag")
playdate.graphics.sprite.getZIndex = make_not_implemented("playdate.graphics.sprite.getZIndex")
playdate.graphics.sprite.isOpaque = make_not_implemented("playdate.graphics.sprite.isOpaque")
playdate.graphics.sprite.isVisible = make_not_implemented("playdate.graphics.sprite.isVisible")
playdate.graphics.sprite.markDirty = make_not_implemented("playdate.graphics.sprite.markDirty")
playdate.graphics.sprite.moveBy = make_not_implemented("playdate.graphics.sprite.moveBy")
playdate.graphics.sprite.moveTo = make_not_implemented("playdate.graphics.sprite.moveTo")
playdate.graphics.sprite.moveWithCollisions = make_not_implemented("playdate.graphics.sprite.moveWithCollisions")
playdate.graphics.sprite.overlappingSprites = make_not_implemented("playdate.graphics.sprite.overlappingSprites")
playdate.graphics.sprite.querySpriteInfoAlongLine = make_not_implemented("playdate.graphics.sprite.querySpriteInfoAlongLine")
playdate.graphics.sprite.querySpritesAlongLine = make_not_implemented("playdate.graphics.sprite.querySpritesAlongLine")
playdate.graphics.sprite.querySpritesAtPoint = make_not_implemented("playdate.graphics.sprite.querySpritesAtPoint")
playdate.graphics.sprite.querySpritesInRect = make_not_implemented("playdate.graphics.sprite.querySpritesInRect")
playdate.graphics.sprite.remove = make_not_implemented("playdate.graphics.sprite.remove")
playdate.graphics.sprite.removeAll = make_not_implemented("playdate.graphics.sprite.removeAll")
playdate.graphics.sprite.removeSprite = make_not_implemented("playdate.graphics.sprite.removeSprite")
playdate.graphics.sprite.removeSprites = make_not_implemented("playdate.graphics.sprite.removeSprites")
playdate.graphics.sprite.resetCollidesWithGroupsMask = make_not_implemented("playdate.graphics.sprite.resetCollidesWithGroupsMask")
playdate.graphics.sprite.resetGroupMask = make_not_implemented("playdate.graphics.sprite.resetGroupMask")
playdate.graphics.sprite.setAlwaysRedraw = make_not_implemented("playdate.graphics.sprite.setAlwaysRedraw")
playdate.graphics.sprite.setBounds = make_not_implemented("playdate.graphics.sprite.setBounds")
playdate.graphics.sprite.setCenter = make_not_implemented("playdate.graphics.sprite.setCenter")
playdate.graphics.sprite.setClipRect = make_not_implemented("playdate.graphics.sprite.setClipRect")
playdate.graphics.sprite.setClipRectsInRange = make_not_implemented("playdate.graphics.sprite.setClipRectsInRange")
playdate.graphics.sprite.setCollideRect = make_not_implemented("playdate.graphics.sprite.setCollideRect")
playdate.graphics.sprite.setCollidesWithGroups = make_not_implemented("playdate.graphics.sprite.setCollidesWithGroups")
playdate.graphics.sprite.setCollidesWithGroupsMask = make_not_implemented("playdate.graphics.sprite.setCollidesWithGroupsMask")
playdate.graphics.sprite.setCollisionsEnabled = make_not_implemented("playdate.graphics.sprite.setCollisionsEnabled")
playdate.graphics.sprite.setGroupMask = make_not_implemented("playdate.graphics.sprite.setGroupMask")
playdate.graphics.sprite.setGroups = make_not_implemented("playdate.graphics.sprite.setGroups")
playdate.graphics.sprite.setIgnoresDrawOffset = make_not_implemented("playdate.graphics.sprite.setIgnoresDrawOffset")
playdate.graphics.sprite.setImage = make_not_implemented("playdate.graphics.sprite.setImage")
playdate.graphics.sprite.setImageDrawMode = make_not_implemented("playdate.graphics.sprite.setImageDrawMode")
playdate.graphics.sprite.setImageFlip = make_not_implemented("playdate.graphics.sprite.setImageFlip")
playdate.graphics.sprite.setOpaque = make_not_implemented("playdate.graphics.sprite.setOpaque")
playdate.graphics.sprite.setRedrawsOnImageChange = make_not_implemented("playdate.graphics.sprite.setRedrawsOnImageChange")
playdate.graphics.sprite.setRotation = make_not_implemented("playdate.graphics.sprite.setRotation")
playdate.graphics.sprite.setScale = make_not_implemented("playdate.graphics.sprite.setScale")
playdate.graphics.sprite.setSize = make_not_implemented("playdate.graphics.sprite.setSize")
playdate.graphics.sprite.setStencilImage = make_not_implemented("playdate.graphics.sprite.setStencilImage")
playdate.graphics.sprite.setStencilPattern = make_not_implemented("playdate.graphics.sprite.setStencilPattern")
playdate.graphics.sprite.setTag = make_not_implemented("playdate.graphics.sprite.setTag")
playdate.graphics.sprite.setTilemap = make_not_implemented("playdate.graphics.sprite.setTilemap")
playdate.graphics.sprite.setUpdatesEnabled = make_not_implemented("playdate.graphics.sprite.setUpdatesEnabled")
playdate.graphics.sprite.setVisible = make_not_implemented("playdate.graphics.sprite.setVisible")
playdate.graphics.sprite.setZIndex = make_not_implemented("playdate.graphics.sprite.setZIndex")
playdate.graphics.sprite.spriteCount = make_not_implemented("playdate.graphics.sprite.spriteCount")
playdate.graphics.sprite.update = make_not_implemented("playdate.graphics.sprite.update")
playdate.graphics.sprite.updatesEnabled = make_not_implemented("playdate.graphics.sprite.updatesEnabled")

--------------------------------------------------------------------------------
--
-- playdate.graphics.tilemap
--
PlaydateObject("playdate.graphics.tilemap")
playdate.graphics.tilemap.__gc = make_not_implemented("playdate.graphics.tilemap.__gc")
playdate.graphics.tilemap.draw = make_not_implemented("playdate.graphics.tilemap.draw")
playdate.graphics.tilemap.getCollisionRects = make_not_implemented("playdate.graphics.tilemap.getCollisionRects")
playdate.graphics.tilemap.getPixelSize = make_not_implemented("playdate.graphics.tilemap.getPixelSize")
playdate.graphics.tilemap.getSize = make_not_implemented("playdate.graphics.tilemap.getSize")
playdate.graphics.tilemap.getTileAtPosition = make_not_implemented("playdate.graphics.tilemap.getTileAtPosition")
playdate.graphics.tilemap.getTileSize = make_not_implemented("playdate.graphics.tilemap.getTileSize")
playdate.graphics.tilemap.getTiles = make_not_implemented("playdate.graphics.tilemap.getTiles")
playdate.graphics.tilemap.setImageTable = make_not_implemented("playdate.graphics.tilemap.setImageTable")
playdate.graphics.tilemap.setSize = make_not_implemented("playdate.graphics.tilemap.setSize")
playdate.graphics.tilemap.setTileAtPosition = make_not_implemented("playdate.graphics.tilemap.setTileAtPosition")
playdate.graphics.tilemap.setTiles = make_not_implemented("playdate.graphics.tilemap.setTiles")

--------------------------------------------------------------------------------
--
-- playdate.graphics.video
--
PlaydateObject("playdate.graphics.video")
playdate.graphics.video.__gc = make_not_implemented("playdate.graphics.video.__gc")
playdate.graphics.video.getContext = make_not_implemented("playdate.graphics.video.getContext")
playdate.graphics.video.getFrameCount = make_not_implemented("playdate.graphics.video.getFrameCount")
playdate.graphics.video.getFrameRate = make_not_implemented("playdate.graphics.video.getFrameRate")
playdate.graphics.video.getSize = make_not_implemented("playdate.graphics.video.getSize")
playdate.graphics.video.renderFrame = make_not_implemented("playdate.graphics.video.renderFrame")
playdate.graphics.video.setContext = make_not_implemented("playdate.graphics.video.setContext")
playdate.graphics.video.useScreenContext = make_not_implemented("playdate.graphics.video.useScreenContext")



--------------------------------------------------------------------------------
--
-- playdate.menu
--
playdate.menu.__name = "playdate.menu"
playdate.menu.addCheckmarkMenuItem = make_not_implemented("playdate.menu.addCheckmarkMenuItem")
playdate.menu.addMenuItem = make_not_implemented("playdate.menu.addMenuItem")
playdate.menu.addOptionsMenuItem = make_not_implemented("playdate.menu.addOptionsMenuItem")
playdate.menu.getMenuItems = make_not_implemented("playdate.menu.getMenuItems")
playdate.menu.item.__gc = make_not_implemented("playdate.menu.item.__gc")
playdate.menu.item.__index = make_not_implemented("playdate.menu.item.__index")
playdate.menu.item.__name = "playdate.menu.item"
playdate.menu.item.__newindex = make_not_implemented("playdate.menu.item.__newindex")
playdate.menu.item.getTitle = make_not_implemented("playdate.menu.item.getTitle")
playdate.menu.item.getValue = make_not_implemented("playdate.menu.item.getValue")
playdate.menu.item.setCallback = make_not_implemented("playdate.menu.item.setCallback")
playdate.menu.item.setTitle = make_not_implemented("playdate.menu.item.setTitle")
playdate.menu.item.setValue = make_not_implemented("playdate.menu.item.setValue")
playdate.menu.removeAllMenuItems = make_not_implemented("playdate.menu.removeAllMenuItems")
playdate.menu.removeMenuItem = make_not_implemented("playdate.menu.removeMenuItem")

--------------------------------------------------------------------------------
--
-- playdate.metadata
--
playdate.metadata.author = "AUTHOR NAME WOULD BE HERE"
playdate.metadata.buildNumber = 100
playdate.metadata.bundleID = "com.example.this-should-be-your-domain-not-example-com.GameNameHere"
playdate.metadata.description = "GAME DESCRIPTION WOULD BE HERE"
playdate.metadata.imagePath = "SystemAssets"
playdate.metadata.name = "GAME NAME WOULD BE HERE"
playdate.metadata.pdxversion = 11200
playdate.metadata.version = "0.1"

--------------------------------------------------------------------------------
--
-- playdate.pathfinder.graph
--
PlaydateObject("playdate.pathfinder.graph")
playdate.pathfinder.graph.__gc = make_not_implemented("playdate.pathfinder.graph.__gc")
playdate.pathfinder.graph.addConnectionToNodeWithID = make_not_implemented("playdate.pathfinder.graph.addConnectionToNodeWithID")
playdate.pathfinder.graph.addConnections = make_not_implemented("playdate.pathfinder.graph.addConnections")
playdate.pathfinder.graph.addNewNode = make_not_implemented("playdate.pathfinder.graph.addNewNode")
playdate.pathfinder.graph.addNewNodes = make_not_implemented("playdate.pathfinder.graph.addNewNodes")
playdate.pathfinder.graph.addNode = make_not_implemented("playdate.pathfinder.graph.addNode")
playdate.pathfinder.graph.addNodes = make_not_implemented("playdate.pathfinder.graph.addNodes")
playdate.pathfinder.graph.allNodes = make_not_implemented("playdate.pathfinder.graph.allNodes")
playdate.pathfinder.graph.findPath = make_not_implemented("playdate.pathfinder.graph.findPath")
playdate.pathfinder.graph.findPathWithIDs = make_not_implemented("playdate.pathfinder.graph.findPathWithIDs")
playdate.pathfinder.graph.new2DGrid = make_not_implemented("playdate.pathfinder.graph.new2DGrid")
playdate.pathfinder.graph.nodeWithID = make_not_implemented("playdate.pathfinder.graph.nodeWithID")
playdate.pathfinder.graph.nodeWithXY = make_not_implemented("playdate.pathfinder.graph.nodeWithXY")
playdate.pathfinder.graph.removeAllConnections = make_not_implemented("playdate.pathfinder.graph.removeAllConnections")
playdate.pathfinder.graph.removeAllConnectionsFromNodeWithID = make_not_implemented("playdate.pathfinder.graph.removeAllConnectionsFromNodeWithID")
playdate.pathfinder.graph.removeNode = make_not_implemented("playdate.pathfinder.graph.removeNode")
playdate.pathfinder.graph.removeNodeWithID = make_not_implemented("playdate.pathfinder.graph.removeNodeWithID")
playdate.pathfinder.graph.removeNodeWithXY = make_not_implemented("playdate.pathfinder.graph.removeNodeWithXY")
playdate.pathfinder.graph.setXYForNodeWithID = make_not_implemented("playdate.pathfinder.graph.setXYForNodeWithID")

--------------------------------------------------------------------------------
--
-- playdate.pathfinder.node
--
playdate.pathfinder.node.__gc = make_not_implemented("playdate.pathfinder.node.__gc")
playdate.pathfinder.node.__index = make_not_implemented("playdate.pathfinder.node.__index")
playdate.pathfinder.node.__name = "playdate.pathfinder.node"
playdate.pathfinder.node.__newindex = make_not_implemented("playdate.pathfinder.node.__newindex")
playdate.pathfinder.node.addConnection = make_not_implemented("playdate.pathfinder.node.addConnection")
playdate.pathfinder.node.addConnectionToNodeWithXY = make_not_implemented("playdate.pathfinder.node.addConnectionToNodeWithXY")
playdate.pathfinder.node.addConnections = make_not_implemented("playdate.pathfinder.node.addConnections")
playdate.pathfinder.node.connectedNodes = make_not_implemented("playdate.pathfinder.node.connectedNodes")
playdate.pathfinder.node.removeAllConnections = make_not_implemented("playdate.pathfinder.node.removeAllConnections")
playdate.pathfinder.node.removeConnection = make_not_implemented("playdate.pathfinder.node.removeConnection")
playdate.pathfinder.node.setXY = make_not_implemented("playdate.pathfinder.node.setXY")

--------------------------------------------------------------------------------
--
-- playdate.scoreboards
--
playdate.scoreboards.addScore = make_not_implemented("playdate.scoreboards.addScore")
playdate.scoreboards.getPersonalBest = make_not_implemented("playdate.scoreboards.getPersonalBest")
playdate.scoreboards.getScoreboards = make_not_implemented("playdate.scoreboards.getScoreboards")
playdate.scoreboards.getScores = make_not_implemented("playdate.scoreboards.getScores")

--------------------------------------------------------------------------------
--
-- playdate.server
--
playdate.server.createMovie = make_not_implemented("playdate.server.createMovie")
playdate.server.getAvatar = make_not_implemented("playdate.server.getAvatar")
playdate.server.setAvatar = make_not_implemented("playdate.server.setAvatar")
playdate.server.uploadFile = make_not_implemented("playdate.server.uploadFile")
playdate.server.uploadImage = make_not_implemented("playdate.server.uploadImage")

--------------------------------------------------------------------------------
--
-- playdate.simulator
--
playdate.simulator.__name = "playdate.simulator"
playdate.simulator.exit = make_not_implemented("playdate.simulator.exit")
playdate.simulator.getURL = make_not_implemented("playdate.simulator.getURL")
playdate.simulator.openURL = make_not_implemented("playdate.simulator.openURL")
playdate.simulator.writeToFile = make_not_implemented("playdate.simulator.writeToFile")

--------------------------------------------------------------------------------
--
-- playdate.sound misc
--
playdate.sound.addEffect = make_not_implemented("playdate.sound.addEffect")
playdate.sound.getCurrentTime = make_not_implemented("playdate.sound.getCurrentTime")
playdate.sound.getHeadphoneState = make_not_implemented("playdate.sound.getHeadphoneState")
playdate.sound.getSampleRate = make_not_implemented("playdate.sound.getSampleRate")
playdate.sound.playingSources = make_not_implemented("playdate.sound.playingSources")
playdate.sound.removeEffect = make_not_implemented("playdate.sound.removeEffect")
playdate.sound.resetTime = make_not_implemented("playdate.sound.resetTime")
playdate.sound.setOutputsActive = make_not_implemented("playdate.sound.setOutputsActive")

--------------------------------------------------------------------------------
--
-- playdate.sound.bitcrusher
--
PlaydateObject("playdate.sound.bitcrusher")
playdate.sound.bitcrusher.__gc = make_not_implemented("playdate.sound.bitcrusher.__gc")
playdate.sound.bitcrusher.setAmount = make_not_implemented("playdate.sound.bitcrusher.setAmount")
playdate.sound.bitcrusher.setAmountMod = make_not_implemented("playdate.sound.bitcrusher.setAmountMod")
playdate.sound.bitcrusher.setMix = make_not_implemented("playdate.sound.bitcrusher.setMix")
playdate.sound.bitcrusher.setMixMod = make_not_implemented("playdate.sound.bitcrusher.setMixMod")
playdate.sound.bitcrusher.setUndersampling = make_not_implemented("playdate.sound.bitcrusher.setUndersampling")
playdate.sound.bitcrusher.setUndersamplingMod = make_not_implemented("playdate.sound.bitcrusher.setUndersamplingMod")

--------------------------------------------------------------------------------
--
-- playdate.sound.channel
--
PlaydateObject("playdate.sound.channel")
playdate.sound.channel.__gc = make_not_implemented("playdate.sound.channel.__gc")
playdate.sound.channel.addEffect = make_not_implemented("playdate.sound.channel.addEffect")
playdate.sound.channel.addSource = make_not_implemented("playdate.sound.channel.addSource")
playdate.sound.channel.getVolume = make_not_implemented("playdate.sound.channel.getVolume")
playdate.sound.channel.remove = make_not_implemented("playdate.sound.channel.remove")
playdate.sound.channel.removeEffect = make_not_implemented("playdate.sound.channel.removeEffect")
playdate.sound.channel.removeSource = make_not_implemented("playdate.sound.channel.removeSource")
playdate.sound.channel.setPan = make_not_implemented("playdate.sound.channel.setPan")
playdate.sound.channel.setPanMod = make_not_implemented("playdate.sound.channel.setPanMod")
playdate.sound.channel.setVolume = make_not_implemented("playdate.sound.channel.setVolume")
playdate.sound.channel.setVolumeMod = make_not_implemented("playdate.sound.channel.setVolumeMod")

--------------------------------------------------------------------------------
--
-- playdate.sound.controlsignal
--
PlaydateObject("playdate.sound.controlsignal")
playdate.sound.controlsignal.__gc = make_not_implemented("playdate.sound.controlsignal.__gc")
--playdate.sound.controlsignal.__index = not_implemented
playdate.sound.controlsignal.__newindex = make_not_implemented("playdate.sound.controlsignal.__newindex")
playdate.sound.controlsignal.addEvent = make_not_implemented("playdate.sound.controlsignal.addEvent")
playdate.sound.controlsignal.clearEvents = make_not_implemented("playdate.sound.controlsignal.clearEvents")
playdate.sound.controlsignal.getControllerType = make_not_implemented("playdate.sound.controlsignal.getControllerType")
playdate.sound.controlsignal.setControllerType = make_not_implemented("playdate.sound.controlsignal.setControllerType")

--------------------------------------------------------------------------------
--
-- playdate.sound.delayline
--
PlaydateObject("playdate.sound.delayline")
playdate.sound.delayline.__gc = make_not_implemented("playdate.sound.delayline.__gc")
playdate.sound.delayline.addTap = make_not_implemented("playdate.sound.delayline.addTap")
playdate.sound.delayline.setFeedback = make_not_implemented("playdate.sound.delayline.setFeedback")
playdate.sound.delayline.setMix = make_not_implemented("playdate.sound.delayline.setMix")
playdate.sound.delayline.setMixMod = make_not_implemented("playdate.sound.delayline.setMixMod")

--------------------------------------------------------------------------------
--
-- playdate.sound.delaylinetap
--
playdate.sound.delaylinetap.__gc = make_not_implemented("playdate.sound.delaylinetap.__gc")
playdate.sound.delaylinetap.__name = "playdate.sound.delaylinetap"
playdate.sound.delaylinetap.getVolume = make_not_implemented("playdate.sound.delaylinetap.getVolume")
playdate.sound.delaylinetap.setDelay = make_not_implemented("playdate.sound.delaylinetap.setDelay")
playdate.sound.delaylinetap.setDelayMod = make_not_implemented("playdate.sound.delaylinetap.setDelayMod")
playdate.sound.delaylinetap.setFlipChannels = make_not_implemented("playdate.sound.delaylinetap.setFlipChannels")
playdate.sound.delaylinetap.setVolume = make_not_implemented("playdate.sound.delaylinetap.setVolume")

--------------------------------------------------------------------------------
--
-- playdate.sound.envelope
--
PlaydateObject("playdate.sound.envelope")
playdate.sound.envelope.setAttack = make_not_implemented("playdate.sound.envelope.setAttack")
playdate.sound.envelope.setDecay = make_not_implemented("playdate.sound.envelope.setDecay")
playdate.sound.envelope.setGlobal = make_not_implemented("playdate.sound.envelope.setGlobal")
playdate.sound.envelope.setLegato = make_not_implemented("playdate.sound.envelope.setLegato")
playdate.sound.envelope.setOffset = make_not_implemented("playdate.sound.envelope.setOffset")
playdate.sound.envelope.setRelease = make_not_implemented("playdate.sound.envelope.setRelease")
playdate.sound.envelope.setRetrigger = make_not_implemented("playdate.sound.envelope.setRetrigger")
playdate.sound.envelope.setScale = make_not_implemented("playdate.sound.envelope.setScale")
playdate.sound.envelope.setSustain = make_not_implemented("playdate.sound.envelope.setSustain")
playdate.sound.envelope.trigger = make_not_implemented("playdate.sound.envelope.trigger")

--------------------------------------------------------------------------------
--
-- playdate.sound.fileplayer
--
PlaydateObject("playdate.sound.fileplayer")
playdate.sound.fileplayer.__gc = make_not_implemented("playdate.sound.fileplayer.__gc")
playdate.sound.fileplayer.didUnderrun = make_not_implemented("playdate.sound.fileplayer.didUnderrun")
playdate.sound.fileplayer.getLength = make_not_implemented("playdate.sound.fileplayer.getLength")
playdate.sound.fileplayer.getOffset = make_not_implemented("playdate.sound.fileplayer.getOffset")
playdate.sound.fileplayer.getRate = make_not_implemented("playdate.sound.fileplayer.getRate")
playdate.sound.fileplayer.getVolume = make_not_implemented("playdate.sound.fileplayer.getVolume")
playdate.sound.fileplayer.isPlaying = make_not_implemented("playdate.sound.fileplayer.isPlaying")
playdate.sound.fileplayer.load = make_not_implemented("playdate.sound.fileplayer.load")
playdate.sound.fileplayer.pause = make_not_implemented("playdate.sound.fileplayer.pause")
playdate.sound.fileplayer.play = make_not_implemented("playdate.sound.fileplayer.play")
playdate.sound.fileplayer.setBufferSize = make_not_implemented("playdate.sound.fileplayer.setBufferSize")
playdate.sound.fileplayer.setFinishCallback = make_not_implemented("playdate.sound.fileplayer.setFinishCallback")
playdate.sound.fileplayer.setLoopCallback = make_not_implemented("playdate.sound.fileplayer.setLoopCallback")
playdate.sound.fileplayer.setLoopRange = make_not_implemented("playdate.sound.fileplayer.setLoopRange")
playdate.sound.fileplayer.setOffset = make_not_implemented("playdate.sound.fileplayer.setOffset")
playdate.sound.fileplayer.setRate = make_not_implemented("playdate.sound.fileplayer.setRate")
playdate.sound.fileplayer.setRateMod = make_not_implemented("playdate.sound.fileplayer.setRateMod")
playdate.sound.fileplayer.setStopOnUnderrun = make_not_implemented("playdate.sound.fileplayer.setStopOnUnderrun")
playdate.sound.fileplayer.setVolume = make_not_implemented("playdate.sound.fileplayer.setVolume")
playdate.sound.fileplayer.stop = make_not_implemented("playdate.sound.fileplayer.stop")

--------------------------------------------------------------------------------
--
-- playdate.sound.instrument
--
PlaydateObject("playdate.sound.instrument")
playdate.sound.instrument.__gc = make_not_implemented("playdate.sound.instrument.__gc")
playdate.sound.instrument.addVoice = make_not_implemented("playdate.sound.instrument.addVoice")
playdate.sound.instrument.allNotesOff = make_not_implemented("playdate.sound.instrument.allNotesOff")
playdate.sound.instrument.getVolume = make_not_implemented("playdate.sound.instrument.getVolume")
playdate.sound.instrument.noteOff = make_not_implemented("playdate.sound.instrument.noteOff")
playdate.sound.instrument.playMIDINote = make_not_implemented("playdate.sound.instrument.playMIDINote")
playdate.sound.instrument.playNote = make_not_implemented("playdate.sound.instrument.playNote")
playdate.sound.instrument.setTranspose = make_not_implemented("playdate.sound.instrument.setTranspose")
playdate.sound.instrument.setVolume = make_not_implemented("playdate.sound.instrument.setVolume")

--------------------------------------------------------------------------------
--
-- playdate.sound.lfo
--
PlaydateObject("playdate.sound.lfo")
playdate.sound.lfo.__gc = make_not_implemented("playdate.sound.lfo.__gc")
playdate.sound.lfo.setArpeggio = make_not_implemented("playdate.sound.lfo.setArpeggio")
playdate.sound.lfo.setCenter = make_not_implemented("playdate.sound.lfo.setCenter")
playdate.sound.lfo.setDelay = make_not_implemented("playdate.sound.lfo.setDelay")
playdate.sound.lfo.setDepth = make_not_implemented("playdate.sound.lfo.setDepth")
playdate.sound.lfo.setGlobal = make_not_implemented("playdate.sound.lfo.setGlobal")
playdate.sound.lfo.setPhase = make_not_implemented("playdate.sound.lfo.setPhase")
playdate.sound.lfo.setRate = make_not_implemented("playdate.sound.lfo.setRate")
playdate.sound.lfo.setRetrigger = make_not_implemented("playdate.sound.lfo.setRetrigger")
playdate.sound.lfo.setType = make_not_implemented("playdate.sound.lfo.setType")

--------------------------------------------------------------------------------
--
-- playdate.sound.micinput
--
playdate.sound.micinput.getLevel = make_not_implemented("playdate.sound.micinput.getLevel")
playdate.sound.micinput.getSource = make_not_implemented("playdate.sound.micinput.getSource")
playdate.sound.micinput.recordToSample = make_not_implemented("playdate.sound.micinput.recordToSample")
playdate.sound.micinput.startListening = make_not_implemented("playdate.sound.micinput.startListening")
playdate.sound.micinput.stopListening = make_not_implemented("playdate.sound.micinput.stopListening")
playdate.sound.micinput.stopRecording = make_not_implemented("playdate.sound.micinput.stopRecording")

--------------------------------------------------------------------------------
--
-- playdate.sound.onepolefilter
--
PlaydateObject("playdate.sound.onepolefilter")
playdate.sound.onepolefilter.__gc = make_not_implemented("playdate.sound.onepolefilter.__gc")
playdate.sound.onepolefilter.setMix = make_not_implemented("playdate.sound.onepolefilter.setMix")
playdate.sound.onepolefilter.setMixMod = make_not_implemented("playdate.sound.onepolefilter.setMixMod")
playdate.sound.onepolefilter.setParameter = make_not_implemented("playdate.sound.onepolefilter.setParameter")
playdate.sound.onepolefilter.setParameterMod = make_not_implemented("playdate.sound.onepolefilter.setParameterMod")

--------------------------------------------------------------------------------
--
-- playdate.sound.overdrive
--
PlaydateObject("playdate.sound.overdrive")
playdate.sound.overdrive.__gc = make_not_implemented("playdate.sound.overdrive.__gc")
playdate.sound.overdrive.setGain = make_not_implemented("playdate.sound.overdrive.setGain")
playdate.sound.overdrive.setLimit = make_not_implemented("playdate.sound.overdrive.setLimit")
playdate.sound.overdrive.setLimitMod = make_not_implemented("playdate.sound.overdrive.setLimitMod")
playdate.sound.overdrive.setMix = make_not_implemented("playdate.sound.overdrive.setMix")
playdate.sound.overdrive.setMixMod = make_not_implemented("playdate.sound.overdrive.setMixMod")
playdate.sound.overdrive.setOffset = make_not_implemented("playdate.sound.overdrive.setOffset")
playdate.sound.overdrive.setOffsetMod = make_not_implemented("playdate.sound.overdrive.setOffsetMod")

--------------------------------------------------------------------------------
--
-- playdate.sound.ringmod
--
PlaydateObject("playdate.sound.ringmod")
playdate.sound.ringmod.__gc = make_not_implemented("playdate.sound.ringmod.__gc")
playdate.sound.ringmod.setFrequency = make_not_implemented("playdate.sound.ringmod.setFrequency")
playdate.sound.ringmod.setFrequencyMod = make_not_implemented("playdate.sound.ringmod.setFrequencyMod")
playdate.sound.ringmod.setMix = make_not_implemented("playdate.sound.ringmod.setMix")
playdate.sound.ringmod.setMixMod = make_not_implemented("playdate.sound.ringmod.setMixMod")

--------------------------------------------------------------------------------
--
-- playdate.sound.sample
--
PlaydateObject("playdate.sound.sample")
playdate.sound.sample.__gc = make_not_implemented("playdate.sound.sample.__gc")
playdate.sound.sample.getFormat = make_not_implemented("playdate.sound.sample.getFormat")
playdate.sound.sample.getLength = make_not_implemented("playdate.sound.sample.getLength")
playdate.sound.sample.getSampleRate = make_not_implemented("playdate.sound.sample.getSampleRate")
playdate.sound.sample.getSubsample = make_not_implemented("playdate.sound.sample.getSubsample")
playdate.sound.sample.load = make_not_implemented("playdate.sound.sample.load")
playdate.sound.sample.play = make_not_implemented("playdate.sound.sample.play")
playdate.sound.sample.playAt = make_not_implemented("playdate.sound.sample.playAt")
playdate.sound.sample.save = make_not_implemented("playdate.sound.sample.save")

--------------------------------------------------------------------------------
--
-- playdate.sound.sampleplayer
--
PlaydateObject("playdate.sound.sampleplayer")
playdate.sound.sampleplayer.__gc = make_not_implemented("playdate.sound.sampleplayer.__gc")
playdate.sound.sampleplayer.copy = make_not_implemented("playdate.sound.sampleplayer.copy")
playdate.sound.sampleplayer.getLength = make_not_implemented("playdate.sound.sampleplayer.getLength")
playdate.sound.sampleplayer.getOffset = make_not_implemented("playdate.sound.sampleplayer.getOffset")
playdate.sound.sampleplayer.getRate = make_not_implemented("playdate.sound.sampleplayer.getRate")
playdate.sound.sampleplayer.getSample = make_not_implemented("playdate.sound.sampleplayer.getSample")
playdate.sound.sampleplayer.getVolume = make_not_implemented("playdate.sound.sampleplayer.getVolume")
playdate.sound.sampleplayer.isPlaying = make_not_implemented("playdate.sound.sampleplayer.isPlaying")
playdate.sound.sampleplayer.play = make_not_implemented("playdate.sound.sampleplayer.play")
playdate.sound.sampleplayer.playAt = make_not_implemented("playdate.sound.sampleplayer.playAt")
playdate.sound.sampleplayer.setFinishCallback = make_not_implemented("playdate.sound.sampleplayer.setFinishCallback")
playdate.sound.sampleplayer.setLoopCallback = make_not_implemented("playdate.sound.sampleplayer.setLoopCallback")
playdate.sound.sampleplayer.setOffset = make_not_implemented("playdate.sound.sampleplayer.setOffset")
playdate.sound.sampleplayer.setPaused = make_not_implemented("playdate.sound.sampleplayer.setPaused")
playdate.sound.sampleplayer.setPlayRange = make_not_implemented("playdate.sound.sampleplayer.setPlayRange")
playdate.sound.sampleplayer.setRate = make_not_implemented("playdate.sound.sampleplayer.setRate")
playdate.sound.sampleplayer.setRateMod = make_not_implemented("playdate.sound.sampleplayer.setRateMod")
playdate.sound.sampleplayer.setSample = make_not_implemented("playdate.sound.sampleplayer.setSample")
playdate.sound.sampleplayer.setVolume = make_not_implemented("playdate.sound.sampleplayer.setVolume")
playdate.sound.sampleplayer.stop = make_not_implemented("playdate.sound.sampleplayer.stop")

--------------------------------------------------------------------------------
--
-- playdate.sound.sequence
--
PlaydateObject("playdate.sound.sequence")
playdate.sound.sequence.__gc = make_not_implemented("playdate.sound.sequence.__gc")
playdate.sound.sequence.addTrack = make_not_implemented("playdate.sound.sequence.addTrack")
playdate.sound.sequence.allNotesOff = make_not_implemented("playdate.sound.sequence.allNotesOff")
playdate.sound.sequence.getCurrentStep = make_not_implemented("playdate.sound.sequence.getCurrentStep")
playdate.sound.sequence.getLength = make_not_implemented("playdate.sound.sequence.getLength")
playdate.sound.sequence.getTempo = make_not_implemented("playdate.sound.sequence.getTempo")
playdate.sound.sequence.getTrackAtIndex = make_not_implemented("playdate.sound.sequence.getTrackAtIndex")
playdate.sound.sequence.getTrackCount = make_not_implemented("playdate.sound.sequence.getTrackCount")
playdate.sound.sequence.goToStep = make_not_implemented("playdate.sound.sequence.goToStep")
playdate.sound.sequence.isPlaying = make_not_implemented("playdate.sound.sequence.isPlaying")
playdate.sound.sequence.play = make_not_implemented("playdate.sound.sequence.play")
playdate.sound.sequence.setLoops = make_not_implemented("playdate.sound.sequence.setLoops")
playdate.sound.sequence.setTempo = make_not_implemented("playdate.sound.sequence.setTempo")
playdate.sound.sequence.setTrackAtIndex = make_not_implemented("playdate.sound.sequence.setTrackAtIndex")
playdate.sound.sequence.stop = make_not_implemented("playdate.sound.sequence.stop")

--------------------------------------------------------------------------------
--
-- playdate.sound.signal
--
playdate.sound.signal.__name = "playdate.sound.signal"
playdate.sound.signal.setOffset = make_not_implemented("playdate.sound.signal.setOffset")
playdate.sound.signal.setScale = make_not_implemented("playdate.sound.signal.setScale")

--------------------------------------------------------------------------------
--
-- playdate.sound.synth
--
PlaydateObject("playdate.sound.synth")
playdate.sound.synth.__gc = make_not_implemented("playdate.sound.synth.__gc")
playdate.sound.synth.copy = make_not_implemented("playdate.sound.synth.copy")
playdate.sound.synth.getVolume = make_not_implemented("playdate.sound.synth.getVolume")
playdate.sound.synth.isPlaying = make_not_implemented("playdate.sound.synth.isPlaying")
playdate.sound.synth.noteOff = make_not_implemented("playdate.sound.synth.noteOff")
playdate.sound.synth.playMIDINote = make_not_implemented("playdate.sound.synth.playMIDINote")
playdate.sound.synth.playNote = make_not_implemented("playdate.sound.synth.playNote")
playdate.sound.synth.setADSR = make_not_implemented("playdate.sound.synth.setADSR")
playdate.sound.synth.setAmplitudeMod = make_not_implemented("playdate.sound.synth.setAmplitudeMod")
playdate.sound.synth.setAttack = make_not_implemented("playdate.sound.synth.setAttack")
playdate.sound.synth.setDecay = make_not_implemented("playdate.sound.synth.setDecay")
playdate.sound.synth.setFinishCallback = make_not_implemented("playdate.sound.synth.setFinishCallback")
playdate.sound.synth.setFrequencyMod = make_not_implemented("playdate.sound.synth.setFrequencyMod")
playdate.sound.synth.setLegato = make_not_implemented("playdate.sound.synth.setLegato")
playdate.sound.synth.setParameter = make_not_implemented("playdate.sound.synth.setParameter")
playdate.sound.synth.setParameterMod = make_not_implemented("playdate.sound.synth.setParameterMod")
playdate.sound.synth.setRelease = make_not_implemented("playdate.sound.synth.setRelease")
playdate.sound.synth.setSustain = make_not_implemented("playdate.sound.synth.setSustain")
playdate.sound.synth.setVolume = make_not_implemented("playdate.sound.synth.setVolume")
playdate.sound.synth.setWaveform = make_not_implemented("playdate.sound.synth.setWaveform")
playdate.sound.synth.stop = make_not_implemented("playdate.sound.synth.stop")

--------------------------------------------------------------------------------
--
-- playdate.sound.track
--
PlaydateObject("playdate.sound.track")
playdate.sound.track.__gc = make_not_implemented("playdate.sound.track.__gc")
playdate.sound.track.addControlSignal = make_not_implemented("playdate.sound.track.addControlSignal")
playdate.sound.track.addNote = make_not_implemented("playdate.sound.track.addNote")
playdate.sound.track.getControlSignals = make_not_implemented("playdate.sound.track.getControlSignals")
playdate.sound.track.getInstrument = make_not_implemented("playdate.sound.track.getInstrument")
playdate.sound.track.getLength = make_not_implemented("playdate.sound.track.getLength")
playdate.sound.track.getNotes = make_not_implemented("playdate.sound.track.getNotes")
playdate.sound.track.getNotesActive = make_not_implemented("playdate.sound.track.getNotesActive")
playdate.sound.track.getPolyphony = make_not_implemented("playdate.sound.track.getPolyphony")
playdate.sound.track.setInstrument = make_not_implemented("playdate.sound.track.setInstrument")
playdate.sound.track.setMuted = make_not_implemented("playdate.sound.track.setMuted")
playdate.sound.track.setNotes = make_not_implemented("playdate.sound.track.setNotes")

--------------------------------------------------------------------------------
--
-- playdate.sound.twopolefilter
--
PlaydateObject("playdate.sound.twopolefilter")
playdate.sound.twopolefilter.__gc = make_not_implemented("playdate.sound.twopolefilter.__gc")
playdate.sound.twopolefilter.setFrequency = make_not_implemented("playdate.sound.twopolefilter.setFrequency")
playdate.sound.twopolefilter.setFrequencyMod = make_not_implemented("playdate.sound.twopolefilter.setFrequencyMod")
playdate.sound.twopolefilter.setGain = make_not_implemented("playdate.sound.twopolefilter.setGain")
playdate.sound.twopolefilter.setMix = make_not_implemented("playdate.sound.twopolefilter.setMix")
playdate.sound.twopolefilter.setMixMod = make_not_implemented("playdate.sound.twopolefilter.setMixMod")
playdate.sound.twopolefilter.setResonance = make_not_implemented("playdate.sound.twopolefilter.setResonance")
playdate.sound.twopolefilter.setResonanceMod = make_not_implemented("playdate.sound.twopolefilter.setResonanceMod")
playdate.sound.twopolefilter.setType = make_not_implemented("playdate.sound.twopolefilter.setType")


--------------------------------------------------------------------------------
--
-- playdate.systemInfo
--
playdate.systemInfo = {
	buildtime = "20000101_000000",
	commit = "fakedate",
	pdxcompatversion = 10000,
	pdxversion = 11200,
	sdk = "1.12.2",
}


--------------------------------------------------------------------------------
--
-- table
--

function table.create(arrayCount,hashCount)
	-- This is returns an empty table. The Playdate implementation
	-- pre-allocates some buffers for table, but it's still an
	-- empty table.
	return {}
end

function table.deepcopy(source)
	assert(type(source) == "table", 
		'The argument passed to table.deepcopy() must be a table (argument passed was a '..type(source)..')')

	local destination = {}
	for k,v in pairs(source) do
		if type(v) == "table" then
			v = table.deepcopy(v)
		end
		destination[k] = v
	end
	return destination
end

local function num_table_entries(tbl)
	local count = 0
	for k,v in pairs(tbl) do
		count = count + 1
	end
	return count
end

function table.getsize(table)
	-- Returns arraySize,hashSize, where those sizes are the
	-- sizes of the BUFFERS allocated, not the actual data.
	-- It's probably only suitable for pre-allocating
	-- space (perhaps with table.create).
	--
	-- The only guarantee is:
	--
	-- local arraysize, hashSize = table.getsize(tbl)
	-- local entries = num_table_entries(tbl)
	-- assert(entries <= (arraySize+hashSize))
	--
	-- It appears hashSize is always at least 1 and is
	-- always a power of 2. "array" entries may end up
	-- in either count.
	--
	-- This is just a crude approximation for what Playdate's
	-- Lua is doing behind the scenes.
	local entries = num_table_entries(table)
	local arraySize = #table
		-- hashSize appears to always be at least 1
	local hashSize = math.max(1,entries - arraySize)
	hashSize = 2^(math.ceil(math.log(hashSize,2)))
	return arraySize, hashSize
end

table.indexOfElement = function(tbl,element)
	if type(element) == "nil" then
		error('indexOfElement can not check for elements of type nil')
	end

	for i,v in ipairs(tbl) do
		if v == element then
			return i
		end
	end
	return nil
end


function table.shallowcopy(source, destination)
	assert(type(source) == "table", 
		-- This is what Playdate SDK 1.12.3 emits.
		'failed expression: (((((t))->tt_) == (((((5) | ((0) << 4))) | (1 << 6))))) && "table expected" at ../../Core/minilua/lapi.c:721')

	if destination == nil then
		destination = {}
	end
	for k,v in pairs(source) do
		destination[k] = v
	end
	return destination
end


--------------------------------------------------------------------------------
--
-- fakedate
--
playdate.fakedate = {
	currentTimeMilliseconds = 0,
	elapsedTimeEpoch = 0,
	refreshRate = 30,
	display = {
		scale = 1
	},
	not_implemented = make_not_implemented("playdate.fakedate.not_implemented")
}

function playdate.fakedate.frame_pass()
	playdate.fakedate.milliseconds_pass(1000/playdate.fakedate.refreshRate)
end

function playdate.fakedate.milliseconds_pass(ms)
	assert(ms >= 0, "Time should not go backward")
	playdate.fakedate.currentTimeMilliseconds = playdate.fakedate.currentTimeMilliseconds + ms
end


local function recursive_count(name, root, depth)
	if depth == nil then
		depth = 0
	end
	if type(root) ~= "table" then
		if type(root) == "function" then
			if _fakedate_not_implemented[name] then
				return 1,0,0
			end
			return 0,1,0
		end
		return 0,0,1
	end
	local not_implemented_count = 0
	local implemented_count = 0
	local constants_count = 0
	local k,v
	for k,v in pairs(root) do
		if k ~= "__index" then
			local sub_not_implemented_count = 0
			local sub_implemented_count = 0
			local sub_implemented_count = 0
			sub_not_implemented_count, sub_implemented_count, sub_constants_count = recursive_count(name.."."..k, v, depth+1)
			not_implemented_count = not_implemented_count + sub_not_implemented_count
			implemented_count = implemented_count + sub_implemented_count
			constants_count = constants_count + sub_constants_count
		end
	end
	return not_implemented_count, implemented_count, constants_count
end

local function get_keys(t)
  local keys={}
  for key,_ in pairs(t) do
    table.insert(keys, key)
  end
  return keys
end

local function report_implementation_status()
	local not_implemented_count = 0
	local total_count = 0
	local to_check = {
		json=json,
		playdate=playdate,
		table=table,
	}
	print("fakedate is active")

	local keys = get_keys(to_check)
	table.sort(keys)

	for idx, name in pairs(keys) do
		local tbl = to_check[name]
		local not_implemented_count = 0
		local implemented_count = 0
		local constants_count = 0
		not_implemented_count, implemented_count, constants_count = recursive_count(name, tbl)
		-- Exclude standard table functions:
		-- concat, insert, move, pack, remove, sort, unpack
		-- print("reporting",name, not_implemented_count, implemented_count, constants_count)
		if name == "table" then
			implemented_count = implemented_count - 7 
		end
		print(string.format("  %-9s %3d/%3d implemented; %3d constants", name..":", implemented_count, implemented_count+not_implemented_count, constants_count))
	end
end

report_implementation_status()
