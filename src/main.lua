

if not import then require "fakedate" end

--if not import then import = require end


local gfx = playdate.graphics


if true then
	r = playdate.geometry.rect.new(10,20,30,40)
	print(r)
	print(r.x, r.y, r.width, r.height)
	print(r.left, r.top, r.right, r.bottom)

	r.x = 1
	r.y = 2
	r.width = 3
	r.height = 4
	print(r)

	r.left = 100
	r.top = 200
	r.right = 300
	r.bottom = 400
end

if false then
	r = import "examplemodule"
	print("examplemodule returned",r)
	print("r.a is",r.a)
end

if false then
	pt = playdate.geometry.point.new(1,2)
	print("pt",pt)
	print("pt:offset(10,20)",pt:offset(10,20))
	print("pt",pt)
	print("pt:offsetBy(100,200)",pt:offsetBy(100,200))
	print("pt",pt)
end

if false then
	pt = playdate.geometry.point.new(3.14,99.99)
	print("pt(3.14, 99.99)",pt)
	print("pt.x",pt.x)
	print("pt.y",pt.y)
	print("pt.dx",pt.dx)
	print("pt.dy",pt.dy)
	print("pt.unpack",pt:unpack())
	print("pt:offset(1,2)", pt:offset(1,2))
	print("  pt is now", pt)
	print("pt:offsetBy(1,2)", pt:offsetBy(3,4))
	print("    pt is now", pt)

	pt3 = playdate.geometry.point.new(3.14,99.99)
	print("pt3", pt3)
	print("pt3:offsetBy(1,2)", pt3:offsetBy(1,2))
	print("pt3", pt3)

	pt1 = playdate.geometry.point.new(10,20)
	print("pt1(10,20)",pt1)
	pt2 = playdate.geometry.point.new(13,16)
	print("pt2(13,16)",pt2)
	print("pt1:squaredDistanceToPoint(pt2)",pt1:squaredDistanceToPoint(pt2))
	print("pt2:squaredDistanceToPoint(pt1)",pt2:squaredDistanceToPoint(pt1))
	print("pt1:distanceToPoint(pt2)",pt1:distanceToPoint(pt2))
	print("pt2:distanceToPoint(pt1)",pt2:distanceToPoint(pt1))
end

--print(table.indexOfElement({
--	aa="aardvark",
--	ba="batman"},"batman"))

--print(table.indexOfElement({"a","an","aardvark", "bacon", "aardman", "animations"}, "aardman"), 5)
--print(table.indexOfElement({"a","an","aardvark", "bacon", "aardman", "animations"}, "aardma"),nil)
--print(table.indexOfElement({}, "aardman"),nil)
--print(table.indexOfElement({"a","an","aardvark", "bacon", 3, "aardman", 10}, 3),5)
--print(table.indexOfElement({"a","an","aardvark", "bacon", 3, "aardman", 10}, "aardman"),6)
--print(table.indexOfElement(3,3),nil)
--print(table.indexOfElement(nil,3),nil)
--print(table.indexOfElement({"a","an","aardvark", "bacon", "aardman", "animations"}, nil),nil)
--print(table.indexOfElement({}, nil),nil)


local last = 0
local spinner = {'⬆️', '➡️', '⬇️', '⬅️'}

if not playdate.isFakedate then
	playdate.display.setRefreshRate(5)
	gfx.drawText("This is a template Playdate program.", 20, 20)
	gfx.drawText("It doesn't really do anything.", 20, 50)
	gfx.drawText("But there is a simple little animation", 20, 80)
	gfx.drawText("so you know it's working", 20, 100)
end



function playdate.update()
	gfx.drawText(spinner[last+1], 195, 145)
	last = (last+1) % 4
end
