if import ~= nil then
	return
end

require "fakedate"

print("fakedatestrict has been loaded. Various standard Lua tables are hidden under stdlua.")

stdlua = {
	arg=arg,
	dofile=dofile,
	io=io,
	loadfile=loadfile,
	os=os,
	package=package,
	require=require,
}

arg = nil
dofile = nil
io = nil
loadfile = nil
os = nil
package = nil
require = nil
