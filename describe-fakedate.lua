#! /usr/bin/lua

-- This file is part of fakedate, an alternate Playdate SDK for testing.
-- Copyright 2022 Alan De Smet
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

require("src/fakedate")

function get_keys(root)
	local keys = {}
	for k,v in pairs(root) do
		table.insert(keys, k)
	end
	return keys
end

function describe(path, root, implemented)
	local keys = get_keys(root)
	table.sort(keys)
	for _, key in ipairs(keys) do
		local var = root[key]
		local fullname = path.."."..key
		if type(var) == "table" then
			describe(fullname, var, implemented)
		else
			local is_implemented = var ~= playdate.fakedate.not_implemented
			if implemented == is_implemented then
				print(fullname)
			end
		end
	end
end

want_implemented = true
if arg[1] == "-n" or arg[1] == "--not-implemented" then
	want_implemented = false
end

to_print = {
	"json",
	"playdate",
	"table",
}

for _,rootname in ipairs(to_print) do
	describe(rootname, _G[rootname], want_implemented)
end


