# This file is from https://gitlab.com/AlanDeSmet/linux-playdate-template

# Your source directory, should contain main.lua and pdxinfo
SRC_DIR = src

# OPTIONAL: extra arguments to pdc and PlaydateSimulator
#PDC_ARGS =
#SIMULATOR_ARGS =

# OPTIONAL: You can set the name of the pdx file to be created here, but you
# don't need to. If you don't, a name will be automatically created from the
# "name" in your $(SRC_DIR)/pdxinfo
#PDX_NAME = YourGameName.pdx

# OPTIONAL: Should contain every single file in your src directory, but you
# don't need to. If you don't, this will be automatically set to every
# file (recursively!) in $(SRC_DIR)
#INFILES = src/main.lua src/pdxinfo src/some_other_file.lua src/SystemAssets/card.png and whatever else

################################################################################
# You can probably ignore everything beyond this point

################################################################################
# Try to automatically determine PDX_NAME and INFILES

ifndef PDX_NAME
	RAW_GAME_NAME = $(shell grep '^name=' src/pdxinfo |head -n1 | sed 's/^name=//' )
	# This is pulled from (and fixes should be pushed back to)
	# ~/src/bourne-shell-misc/make-safe-filename.sh
	SAFE_GAME_NAME = $(shell printf "%s" $(RAW_GAME_NAME) | tr '\n\r' '-' \
					| sed -e 's/[^[:alnum:]_.-]/-/g' \
							-e 's/^-*//' \
							-e 's/-*$$//' \
							-e 's/--*/-/' \
							-e 's/^[.]*//')
	PDX_NAME = $(SAFE_GAME_NAME).pdx

	# You probably don't want to manually set this
endif

ifndef INFILES
	INFILES = $(shell find $(SRC_DIR) -type f)
endif


################################################################################
# Figure out where the SDK binaries are

osname := $(shell uname)

# On Linux, we'll just rely on PLAYDATE_SDK_PATH
ifeq ($(osname),Linux)
	REALOS = Linux
	ifndef PLAYDATE_SDK_PATH
		$(error PLAYDATE_SDK_PATH is not set)
	endif
	PDCBIN = $(PLAYDATE_SDK_PATH)/bin/pdc
	SIMBIN = $(PLAYDATE_SDK_PATH)/bin/PlaydateSimulator
endif

# On MacOS we'll try to find the binaries in the same way Panic's example
# makefile does
ifeq ($(osname),Darwin)
	REALOS = 'Darwin (probably MacOS)'
	ifndef PLAYDATE_SDK_PATH
		# Next line is from Panic's example makefile
		PLAYDATE_SDK_PATH = $(shell egrep '^\s*SDKRoot' ~/.Playdate/config | head -n 1 | cut -c9-)
	endif
	PDCBIN = '$(PLAYDATE_SDK_PATH)/bin/pdc'
	SIM = Playdate Simulator
	# Next line is from Panic's example makefile; it seems overkill
	#SIMBIN = open -a '$(PLAYDATE_SDK_PATH)/bin/$(SIM).app/Contents/MacOS/$(SIM)'
	SIMBIN = '$(PLAYDATE_SDK_PATH)/bin/$(SIM).app/Contents/MacOS/$(SIM)'
endif

################################################################################
# Make sure we have plausible command paths

FOUND_PDC = $(shell which $(PDCBIN))
ifneq ($(PDCBIN),$(FOUND_PDC))
$(error pdc binary not at $(PDCBIN))
endif

FOUND_SIM = $(shell which $(SIMBIN))
ifneq ($(SIMBIN),$(FOUND_SIM))
$(error PlaydateSimulator binary not at $(SIMBIN))
endif


################################################################################
# Actual build rules

$(PDX_NAME): $(INFILES)
	$(PDCBIN) $(PDC_ARGS) $(SRC_DIR) $(PDX_NAME)

run: $(PDX_NAME)
	$(SIMBIN) $(SIMULATOR_ARGS) $(PDX_NAME)

clean:
	rm -rf $(PDX_NAME)

################################################################################
# These are for compatibility with the Examples/Game Template/makefile Panic shipped.
build: clean compile run

compile: $(PDX_NAME)

open: run


################################################################################
# Check what we found for paths 
makeinfo:
	@echo 'Operating system:   $(REALOS)'
	@echo 'Using pdc in:       $(PDCBIN)'
	@echo 'Using simulator in: $(SIMBIN)'
	@echo 'Output file:        $(PDX_NAME)'
	@echo 'Input directory:    $(SRC_DIR)'
	@echo 'Input files:        $(INFILES)'



nocommitobject.lua:
	sed -E 's/([^ \t]+)[ \t]*([-+*/%&|^])=/\1 = \1 \2/' "$(PLAYDATE_SDK_PATH)/CoreLibs/object.lua" > $@
