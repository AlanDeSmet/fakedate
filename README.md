fakedate: an alternate Playdate SDK for testing
===============================================

fakedate is an alternate implemenation of the Playdate Lua SDK, intended for
testing. It is designed to be compatible with any interpreter compatible with
Lua 5.4.

Currently only a small subset of the Playdate SDK is implemented: the objects and
functions I needed for my own tests, constants which were trivial to implement,
and some arbirary other functionality I decided to implement.

As of writing, 207 of the 864 entries in `playdate` are implemented, all 5 of the `table` extensions are implemented, and none of the 7 json entries are implemented. fakedate will emit current counts on load; you can `lua test-fakedate.lua` to see current counts. You can also use `lua describe-fakedate.lua` to see which things are implemented. `lua describe-fakedate.lua -n` will show what is not implemented.

Note that the counts can be misleading. In particular, many objects have `new` implemented, but `new()` will fail as my implementation relies on `init()` existing.



Playdate's CoreLibs
-------------------

Generally functionality present in Playdate's CoreLibs is _not_ implemented.  These can be difficult to get workind under a standard Lua interpreter. I was able to use `object.lua` by creating as modified version. You can use `make nocommitojbect.lua` (or
`sed -E 's/([^ \t]+)[ \t]*([-+*/%&|^])=/\1 = \1 \2/' "$PLAYDATE_SDK_PATH/CoreLibs/object.lua" > nocommitobject.lua`) to create a matching one.

Usage
-----

To use fakedate, place this at the top of your Lua code:

```
if not import then require "fakedate" end
```

The internal clocks are faked; you can cause time to pass by calling `playdate.fakedate.milliseconds_pass(ms)` or `playdate.fakedate.frame_pass()` which will add the suitable number of milliseconds for a given refresh rate.


Copyright
---------

This file is part of fakedate, an alternate Playdate SDK for testing.
Copyright 2022 Alan De Smet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
